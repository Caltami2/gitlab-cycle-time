DOCKER       = docker
DOCKER_IMAGE = cdp-cycle-time
DOCKER_RUN   = $(DOCKER) container run

.PHONY: all build 

all: lint test build 

test:
	cd src && CGO_ENABLED=0 go test -v -cover -gl-token=${GITLAB_API_TOKEN} -es-domain=http://localhost:9200

lint:
	golint -set_exit_status=1 -min_confidence 0.8 ./src/

build:
	cd src && CGO_ENABLED=0 go build -o ${CI_PROJECT_DIR}/gitlab-cycle-time

elasticsearch:
	docker run --name es --env "discovery.type=single-node" --env "xpack.security.enabled=false" --hostname elasticsearch --publish 9200:9200 --publish 9300:9300 -d docker.elastic.co/elasticsearch/elasticsearch:5.6.8

docker-all: docker-image docker-build

docker-image:
	$(DOCKER) build . --tag $(DOCKER_IMAGE)

docker-build:
	$(DOCKER_RUN) $(DOCKER_IMAGE)

