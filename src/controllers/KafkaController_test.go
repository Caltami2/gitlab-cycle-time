package controllers

import (
	"github.com/Shopify/sarama"
	"github.com/abdallahmaltamimi/go-gitlab"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/mock"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)
func TestKafkaController_StartConsumerGroup(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	//mockConsumerService := mock.NewMockkafkaConsumerServiceInterface(mockCtrl)
	//mockConsumerGroup := mock.NewMockConsumerGroup(mockCtrl)


}

func TestKafkaController_SetReactor(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()


	reactor := &MessageReactor{}

	consumer := &KafkaController{
		reactor: reactor,
	}

	consumer.SetReactor(reactor)

	assert.Equal(t, reactor, consumer.reactor, "Assert reactor is set properly after calling Set reactor")

}

func TestKafkaController_SetVerbosity(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCycleTime := mock.MockCycleTimeControllerInterface{}


	reactor := MessageReactor{cycleTimeCalculator: &mockCycleTime,}
	consumer := &KafkaController{
		reactor: &reactor,
	}
	consumer.SetVerbosity(true)

	assert.Equal(t, true, consumer.verbose, "Assert reactor is set properly after calling Set reactor")

}



func TestKafkaController_InitializeConfiguration(t *testing.T) {
	type fields struct {
		config  sarama.Config
		domain  string
		verbose bool
		brokers string
		group   string
		topics  string
		reactor MessageReactor
	}
	type args struct {
		brokers  string
		topics   string
		group    string
		version  string
		assignor string
		oldest   bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "No Group",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "",
				version:  "v1.0.0",
				assignor: "sticky",
				oldest:   false,
			},
			wantErr: true,
		},
		{
			name: "No Broker",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "v1.0.0",
				assignor: "sticky",
				oldest:   false,
			},
			wantErr: true,
		},
		{
			name: "No Topic",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "",
				group:    "notEmpty",
				version:  "v1.0.0",
				assignor: "sticky",
				oldest:   false,
			},
			wantErr: true,
		},
		{
			name: "Bad Kafka Version",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "",
				assignor: "sticky",
				oldest:   false,
			},
			wantErr: true,
		},
		{
			name: "Bad Assignor",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "2.1.1",
				assignor: "",
				oldest:   false,
			},
			wantErr: true,
		},
		{
			name: "Sticky Assignor",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "2.1.1",
				assignor: "sticky",
				oldest:   false,
			},
			wantErr: false,
		},
		{
			name: "RoundRobin Assignor",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "2.1.1",
				assignor: "roundrobin",
				oldest:   false,
			},
			wantErr: false,
		},
		{
			name: "Range Assignor",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "2.1.1",
				assignor: "range",
				oldest:   false,
			},
			wantErr: false,
		},
		{
			name: "Oldest true",
			fields: fields{
				config:  sarama.Config{},
				domain:  "",
				verbose: false,
				brokers: "",
				group:   "",
				topics:  "",
				reactor: MessageReactor{},
			},
			args: args{
				brokers:  "notEmpty",
				topics:   "notEmpty",
				group:    "notEmpty",
				version:  "2.1.1",
				assignor: "range",
				oldest:   true,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &KafkaController{
				config:  tt.fields.config,
				domain:  tt.fields.domain,
				verbose: tt.fields.verbose,
				brokers: tt.fields.brokers,
				group:   tt.fields.group,
				topics:  tt.fields.topics,
				reactor: &tt.fields.reactor,
			}
			if err := k.InitializeConfiguration(tt.args.brokers, tt.args.topics, tt.args.group, tt.args.version, tt.args.assignor, tt.args.oldest); (err != nil) != tt.wantErr {
				t.Errorf("InitializeConfiguration() error = %v, wantErr %v", err, tt.wantErr)
			}

		})
	}
}

func TestKafkaController_InitializeConsumer(t *testing.T) {
	type fields struct {
		config          sarama.Config
		domain          string
		verbose         bool
		brokers         string
		group           string
		topics          string
		reactor         MessageReactor
		consumerService services.KafkaConsumerService
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
		name: "Assert consumerService is initialized.",
		fields: fields{
			config:  sarama.Config{},
			domain:  "",
			verbose: false,
			brokers: "",
			group:   "",
			topics:  "",
			reactor: MessageReactor{},
		},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &KafkaController{
				config:          tt.fields.config,
				domain:          tt.fields.domain,
				verbose:         tt.fields.verbose,
				brokers:         tt.fields.brokers,
				group:           tt.fields.group,
				topics:          tt.fields.topics,
				reactor:         &tt.fields.reactor,
			}
			assert.Equal(t, chan bool((chan bool)(nil)), *k.consumerService.GetReady(), "Consumer service must be initialized during functiona call" )

			k.initializeConsumer()

			assert.NotEqual(t,  chan bool((chan bool)(nil)), *k.consumerService.GetReady(), "Consumer service must be initialized during functiona call" )
		})
	}
}

// teardown closes the test HTTP server.
func teardown(server *httptest.Server) {
	server.Close()
}

func testMethod(t *testing.T, r *http.Request, want string) {
	if got := r.Method; got != want {
		t.Errorf("Request method: %s, want %s", got, want)
	}
}

func writeHTTPResponse(t *testing.T, w http.ResponseWriter, s string) {
	f, err := os.Open(s)
	if err != nil {
		t.Fatalf("error opening fixture file: %v", err)
	}

	if _, err = io.Copy(w, f); err != nil {
		t.Fatalf("error writing response: %v", err)
	}
}

func setup() (*http.ServeMux, *httptest.Server, *gitlab.Client) {
	// mux is the HTTP request multiplexer used with the test server.
	mux := http.NewServeMux()

	// server is a test HTTP server used to provide mock API responses.
	server := httptest.NewServer(mux)

	// client is the Gitlab client being tested.
	client := gitlab.NewClient(nil, "")
	client.SetBaseURL(server.URL)

	return mux, server, client
}