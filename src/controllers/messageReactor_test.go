package controllers

import (
	"github.com/Shopify/sarama"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/mock"
	"testing"
	"time"
)

func TestMessageReactor_React(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCycleTimeController := mock.NewMockCycleTimeControllerInterface(mockCtrl)

	testMergeRequestMessage :=  sarama.ConsumerMessage{
		Headers:        nil,
		Timestamp:      time.Time{},
		BlockTimestamp: time.Time{},
		Key:            []byte{},
		Value:          []byte{},
		Topic:          "cdp-gitlab-webhook-merge-requests",
		Partition:      0,
		Offset:         0,
	}
	testPipelineMessage :=  sarama.ConsumerMessage{
		Headers:        nil,
		Timestamp:      time.Time{},
		BlockTimestamp: time.Time{},
		Key:            []byte{},
		Value:          []byte{},
		Topic:          "cdp-gitlab-webhook-pipelines",
		Partition:      0,
		Offset:         0,
	}
	testNoTopicMessage :=  sarama.ConsumerMessage{
		Headers:        nil,
		Timestamp:      time.Time{},
		BlockTimestamp: time.Time{},
		Key:            []byte{},
		Value:          []byte{},
		Topic:          "",
		Partition:      0,
		Offset:         0,
	}
	type fields struct {
		cycleTimeCalculator CycleTimeControllerInterface
	}
	type args struct {
		message sarama.ConsumerMessage
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Return no error for expected merge request message",
			fields: fields {
				cycleTimeCalculator: mockCycleTimeController,
			},
			args : args {
				message: testMergeRequestMessage,
			},
			wantErr: false,
		},
		{
			name: "Return no error for expected pipeline message",
			fields: fields {
				cycleTimeCalculator: mockCycleTimeController,
			},
			args : args {
				message: testPipelineMessage,
			},
			wantErr: false,
		},
		{
			name: "Return no error for expected pipeline message",
			fields: fields {
				cycleTimeCalculator: mockCycleTimeController,
			},
			args : args {
				message: testNoTopicMessage,
			},
			wantErr: true,
		},

	}
	mockCycleTimeController.EXPECT().HandlePipelineMessage(testPipelineMessage)
	mockCycleTimeController.EXPECT().HandleMergeRequestMessage(testMergeRequestMessage)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reactor := &MessageReactor{
				cycleTimeCalculator: tt.fields.cycleTimeCalculator,
			}
			if err := reactor.React(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("React() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMessageReactor(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCycleTimeController := mock.NewMockCycleTimeControllerInterface(mockCtrl)

	messageReactor := NewMessageReactor(mockCycleTimeController)

	assert.Equal(t,mockCycleTimeController, messageReactor.cycleTimeCalculator, "Cycle time calculator should be set during NewMessageReactor" )
}