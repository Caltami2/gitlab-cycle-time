package controllers

import (
	"github.com/abdallahmaltamimi/go-gitlab"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/structs"
	"log"
	"net/http"
	"testing"
	"time"
)


func TestGitlabController_NewService(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()


}

func TestGitlabController_TestCheckIfSuccessfulBuildToProd(t *testing.T) {
	type args struct {
		build structs.Build
		pl    structs.PipelineEvent
	}
	var tests = []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Correct Stage, Correct Environment, But Build Failed ",
			args: args{
				build: structs.Build{
					Stage:  "deploy",
					Status: "failure",
					Name: "deploy-prod",
				},
				pl: structs.PipelineEvent{
						ObjectAttributes: structs.PipelineObjectAttributes{
							Variables: []map[string]string{
								{
									"ENV": "Production",
								},
							},
						},
				},
			},
			want: false,
		},
		{
			name: "Correct Environment, Build Succeeds, but wrong Stage ",
			args: args{
				build: structs.Build{
					Stage:  "package",
					Status: "failure",
					Name: "package-docker-dev-auto",
				},
				pl: structs.PipelineEvent{
					ObjectAttributes: structs.PipelineObjectAttributes{
						Variables: []map[string]string{
							{
								"ENV": "Production",
							},
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Correct Stage, Build Succeeds, But wrong environment. ",
			args: args{
				build: structs.Build{
					Stage:  "deploy",
					Status: "failure",
					Name: "deploy-pre-prod",
				},
				pl: structs.PipelineEvent{
					ObjectAttributes: structs.PipelineObjectAttributes{
						Variables: []map[string]string{
							{
								"ENV": "Production",
							},
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Correct Stage, Build Succeeds, And correct Environment ",
			args: args{
				build: structs.Build{
					Stage:  "deploy",
					Status: "success",
					Name: "deploy-prod",
				},
				pl: structs.PipelineEvent{
					ObjectAttributes: structs.PipelineObjectAttributes{
						Variables: []map[string]string{
							{
								"ENV": "Production",
							},
						},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			build := tt.args.build
			if got := CheckIfSuccessfulBuildToProd(&build, tt.args.pl); got != tt.want {
				t.Errorf("CheckIfSuccessfulBuildToProd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGitlabController_TestFilterStringForProduction(t *testing.T) {
	tests := []struct {
		name string
		arg string
		want bool
	}{
		{
			name: "Testing Valid Production values",
			arg: "prod",
			want: true,
		},
		{
			name: "Testing Valid Production values",
			arg: "prd",
			want: true,
		},
		{
			name: "Testing Valid Production values",
			arg: "production",
			want: true,
		},
		{
			name: "Testing Invalid Production values",
			arg: "pre-production",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "preproduction",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "pre-prod",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "preprod",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "staging",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "stage",
			want: false,
		},
		{
			name: "Testing Invalid Production values",
			arg: "something_way_off",
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FilterStringForProduction(tt.arg); got != tt.want {
				t.Errorf("FilterStringForProduction() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestGitlabController_TestGetTimeStampOfLastDeployToProdFromListOfDeploys_NilList(t *testing.T) {
	deployments := []*gitlab.Deployment{}

	list := GetTimeStampOfLastDeployToProdFromListOfDeploys(deployments)

	assert.Equal(t, (*time.Time)(nil) , list, "Asserting nothing is returned from an empty list.")

}
func TestGitlabController_TestGetTimeStampOfLastDeployToProdFromListOfDeploys(t *testing.T) {
	comparisonTimeStamp := time.Now()


	deployment1 := gitlab.Deployment{
		ID:          0,
		IID:         0,
		Ref:         "",
		SHA:         "",
		CreatedAt:   nil,
		User:        nil,
		Environment: &gitlab.Environment{
			ID:             0,
			Name:           "staging",
		},
		Deployable: struct {
			ID         int            `json:"id"`
			Status     string         `json:"status"`
			Stage      string         `json:"stage"`
			Name       string         `json:"name"`
			Ref        string         `json:"ref"`
			Tag        bool           `json:"tag"`
			Coverage   float64        `json:"coverage"`
			CreatedAt  *time.Time     `json:"created_at"`
			StartedAt  *time.Time     `json:"started_at"`
			FinishedAt *time.Time     `json:"finished_at"`
			Duration   float64        `json:"duration"`
			User       *gitlab.User   `json:"user"`
			Commit     *gitlab.Commit `json:"commit"`
			Pipeline   struct {
				ID     int    `json:"id"`
				SHA    string `json:"sha"`
				Ref    string `json:"ref"`
				Status string `json:"status"`
			} `json:"pipeline"`
			Runner *gitlab.Runner `json:"runner"`
		}{
			FinishedAt: &time.Time{},
			Status: "success",
		},
	}

	deployment2 := gitlab.Deployment{
		ID:          0,
		IID:         0,
		Ref:         "",
		SHA:         "",
		CreatedAt:   nil,
		User:        nil,
		Environment: &gitlab.Environment{
			ID:             0,
			Name:           "prod",
		},
		Deployable: struct {
			ID         int            `json:"id"`
			Status     string         `json:"status"`
			Stage      string         `json:"stage"`
			Name       string         `json:"name"`
			Ref        string         `json:"ref"`
			Tag        bool           `json:"tag"`
			Coverage   float64        `json:"coverage"`
			CreatedAt  *time.Time     `json:"created_at"`
			StartedAt  *time.Time     `json:"started_at"`
			FinishedAt *time.Time     `json:"finished_at"`
			Duration   float64        `json:"duration"`
			User       *gitlab.User   `json:"user"`
			Commit     *gitlab.Commit `json:"commit"`
			Pipeline   struct {
				ID     int    `json:"id"`
				SHA    string `json:"sha"`
				Ref    string `json:"ref"`
				Status string `json:"status"`
			} `json:"pipeline"`
			Runner *gitlab.Runner `json:"runner"`
		}{
			FinishedAt: &comparisonTimeStamp,
			Status: "success",
		},
	}

	deploys := []*gitlab.Deployment{
		&deployment1,
		&deployment2,
	}

	resultTime := GetTimeStampOfLastDeployToProdFromListOfDeploys(deploys)

	assert.Equal(t, &comparisonTimeStamp, resultTime, "Asserting that the time returned belongs to the correct pipeline")

	deploys = []*gitlab.Deployment{
		&deployment1,
	}

	resultTime = GetTimeStampOfLastDeployToProdFromListOfDeploys(deploys)

	assert.Equal(t, &time.Time{}, resultTime, "Asserting that the time returned belongs to the correct pipeline")

}


func TestGitlabController_TestNewGitlabController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	testController := NewGitlabController("TEST_TOKEN_1")

	assert.Equal(t, "TEST_TOKEN_1", testController.apiToken, "Asserting that api token is set during creation of new GitlabController")
}

func TestGitlabController_TestNewService(t *testing.T){
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	testController := NewGitlabController("")

	assertPanic(t, func(){testController.NewService()})
}

func TestGitlabController_TestNewService_success(t *testing.T){
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	testController := NewGitlabController("Non_Empty_Value")

	err := testController.NewService()

	assert.NoErrorf(t, err, "Asserting No rrror is returned when apiToken is set ")
}

func TestGetJiraTicketsFromAListOfCommits(t *testing.T) {
	timeNow := time.Now()

	commit1 := 	gitlab.Commit{
		ID:             "",
		ShortID:        "",
		Title:          "TEQCDP-123456",
		AuthorName:     "Some Guy",
		AuthorEmail:    "someguy@gmail.com",
		AuthoredDate:   &timeNow,
		CommitterName:  "",
		CommitterEmail: "",
		CommittedDate:  &timeNow,
		CreatedAt:      nil,
		Message:        "",
		ParentIDs:      nil,
		Stats:          nil,
		Status:         nil,
		LastPipeline:   nil,
		ProjectID:      0,
	}

	commit2 := 	gitlab.Commit{
		ID:             "",
		ShortID:        "",
		Title:          "TEQCDP-123456",
		AuthorName:     "Some Guy",
		AuthorEmail:    "someguy@gmail.com",
		AuthoredDate:   &timeNow,
		CommitterName:  "",
		CommitterEmail: "",
		CommittedDate:  &timeNow,
		CreatedAt:      nil,
		Message:        "",
		ParentIDs:      nil,
		Stats:          nil,
		Status:         nil,
		LastPipeline:   nil,
		ProjectID:      0,
	}


	commits := []*gitlab.Commit{
		&commit1,
		&commit2,
	}

	list := GetJiraTicketsFromAListOfCommits(commits)

	assert.Contains(t,list,"TEQCDP-123456")

	//Assert two tickets with the same name are not added twice.

	assert.Len(t, list, 1)

}

func TestPipelineEventVariablesContainProductionEnv(t *testing.T) {
	type args struct {
		pl structs.PipelineEvent
	}
	tests := []struct {
		name string
		args args
		want bool
	}{{
		name:"",
		args: args {
			pl: structs.PipelineEvent{
				ObjectKind:       "Return error with no pipelineObjectVariables",
				ObjectAttributes: structs.PipelineObjectAttributes{
					ID:             0,
					IID:            0,
					Ref:            "",
					Tag:            false,
					Sha:            "",
					BeforeSha:      "",
					Source:         "",
					Status:         "",
					DetailedStatus: "",
					Stages:         nil,
					CreatedAt:      time.Time{},
					FinishedAt:     time.Time{},
					Duration:       0,
					Variables:      nil,
				},
				Builds:           nil,
			},
		},
		want: false,

	},{
		name:"",
		args: args {
			pl: structs.PipelineEvent{
				ObjectKind:       "Return no error with pipelineObjectVariables",
				ObjectAttributes: structs.PipelineObjectAttributes {
					ID:             0,
					IID:            0,
					Ref:            "",
					Tag:            false,
					Sha:            "",
					BeforeSha:      "",
					Source:         "",
					Status:         "",
					DetailedStatus: "",
					Stages:         nil,
					CreatedAt:      time.Time{},
					FinishedAt:     time.Time{},
					Duration:       0,
					Variables:     []map[string]string{
						{
							"ENV":"prod",
						},
					} ,
				},
				Builds:           nil,
			},
		},
		want: true,

	}, {
			name:"",
			args: args {
				pl: structs.PipelineEvent{
					ObjectKind:       "Return no error with pipelineObjectVariables",
					ObjectAttributes: structs.PipelineObjectAttributes {
						ID:             0,
						IID:            0,
						Ref:            "",
						Tag:            false,
						Sha:            "",
						BeforeSha:      "",
						Source:         "",
						Status:         "",
						DetailedStatus: "",
						Stages:         nil,
						CreatedAt:      time.Time{},
						FinishedAt:     time.Time{},
						Duration:       0,
						Variables:     []map[string]string{
							{
								"ENV":"stage",
							},
						} ,
					},
					Builds:           nil,
				},
			},
			want: false,

		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PipelineEventVariablesContainProductionEnv(tt.args.pl); got != tt.want {
				t.Errorf("PipelineEventVariablesContainProductionEnv() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGitlabController_GetCommitsBetweenTwoTimeStamps_valid(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/repository/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_commits_between_timestamps.json")
	})

	timestamp1 := time.Now()

	timestamp2 := time.Now()
	client.SetBaseURL(server.URL)
	gitlabAPIService := services.GitLabAPIService{Client: client}
	gitlabController := &GitlabController{apiToken:"TEST_TOKEN", apiService:&gitlabAPIService}

	_, err := gitlabController.GetCommitsBetweenTwoTimeStamps(12345, &timestamp1, &timestamp2)

	assert.NoErrorf(t, err, "No error should be returned ")


}
func TestGitlabController_GetCommitsBetweenTwoTimeStamps_invalid(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/repository/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_commits_between_timestamps.json")
	})

	timestamp1 := time.Now()

	timestamp2 := time.Now()
	gitlabAPIService := services.GitLabAPIService{Client: client}
	gitlabController := &GitlabController{apiToken:"", apiService:&gitlabAPIService}

	answer, err := gitlabController.GetCommitsBetweenTwoTimeStamps(123, &timestamp1, &timestamp2)

	assert.Errorf(t, err, "Error should be returned ")

	log.Printf("%v",answer)


}