package controllers

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/mock"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/models"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"testing"
	"time"
)

func TestElasticSearchController_CreateIndex_ErrorWhenIndexExists(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	mapping := `{
				"mappings":{
				"cycle_time":{
					"properties":{
						"time_stamp":{
							"type":"date"
						},
						"ticket":{
							"type":"keyword"
						}
					}
				}
			}
		}`

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	mockElasticService.EXPECT().IndexExists(e.Index).Return(true)

	err :=	e.CreateIndex(e.Index,mapping)

	assert.Errorf(t,err,"Error expected after insertIndex when the Index exists, application handles gracefully.")

}

func TestElasticSearchController_CreateIndex_Success(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	mapping := `{
				"mappings":{
				"cycle_time":{
					"properties":{
						"time_stamp":{
							"type":"date"
						},
						"ticket":{
							"type":"keyword"
						}
					}
				}
			}
		}`

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	mockElasticService.EXPECT().IndexExists(e.Index).Return(false)

	mockElasticService.EXPECT().CreateIndex(e.Index,mapping).Return(nil)

	err :=	e.CreateIndex(e.Index,mapping)

	assert.NoErrorf(t,err,"No error expected after insertIndex when the Index doesn't exist, application handles gracefully.")

}

func TestElasticSearchController_InsertTicket_WhenIndexDoesntExist(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	time := time.Time{}.UTC()

	ticket := models.Ticket{
		TicketName: "test_ticket",
		TimeStamp:  &time,
	}
	mockElasticService.EXPECT().IndexExists(e.Index).Return(false)

	err :=	e.InsertTicket(ticket, e.Index)

	assert.Errorf(t,err,"Error expected after insertIndex when the Index doesn't exist, application handles gracefully.")
}

func TestElasticSearchController_InsertTicket_WhenIndexExists(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}
	time := time.Time{}.UTC()

	ticket := models.Ticket{
		TicketName: "test_ticket",
		TimeStamp:  &time,
	}
	mockElasticService.EXPECT().IndexExists(e.Index).Return(true)

	mockElasticService.EXPECT().Insert(ticket, e.recordType, e.Index).Return(nil)

	err :=	e.InsertTicket(ticket, e.Index)

	assert.NoErrorf(t,err,"No error expected after insertIndex when the Index doesn't exist, application handles gracefully.")

}
func TestElasticSearchController_InsertTickets_whenIndexDoesntExist(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}
	time := time.Now()

	firstTestTicket := models.Ticket{
		TicketName: "test_ticket_1.stopped",
		TimeStamp:  &time,
	}
	secondTestTicket := models.Ticket{
		TicketName: "test_ticket_2.stopped",
		TimeStamp:  &time,
	}
	testRecords := []models.Ticket{
		firstTestTicket,
		secondTestTicket,
	}
	mockElasticService.EXPECT().IndexExists(e.Index).Return(false)

	err :=	e.InsertTickets(testRecords, e.Index)

	assert.Errorf(t,err,"If Index Doesnt exist, then an error is expected after insertIndex application handles gracefully.")

}
func TestElasticSearchController_InsertTickets_whenIndexExists(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}
	time := time.Now()

	firstTestTicket := models.Ticket{
		TicketName: "test_ticket_1.stopped",
		TimeStamp:  &time,
	}
	secondTestTicket := models.Ticket{
		TicketName: "test_ticket_2.stopped",
		TimeStamp:  &time,
	}
	testRecords := []models.Ticket{
		firstTestTicket,
		secondTestTicket,
	}
	mockElasticService.EXPECT().IndexExists(e.Index).Return(true)

	mockElasticService.EXPECT().Insert(firstTestTicket, e.recordType, e.Index).Return(nil)

	mockElasticService.EXPECT().Insert(secondTestTicket, e.recordType, e.Index).Return(nil)

	err :=	e.InsertTickets(testRecords, e.Index)

	assert.NoErrorf(t,err,"No error expected after insertIndex when the Index doesn't exist, application handles gracefully.")

}
func TestElasticSearchController_InsertTickets_whenIndexExists_ButElasticSearchReturnsError(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}
	time := time.Now()

	firstTestTicket := models.Ticket{
		TicketName: "test_ticket_1.stopped",
		TimeStamp:  &time,
	}
	secondTestTicket := models.Ticket{
		TicketName: "test_ticket_2.stopped",
		TimeStamp:  &time,
	}
	testRecords := []models.Ticket{
		firstTestTicket,
		secondTestTicket,
	}
	mockElasticService.EXPECT().IndexExists(e.Index).Return(true)

	mockElasticService.EXPECT().Insert(firstTestTicket, e.recordType, e.Index).Return(nil)

	mockElasticService.EXPECT().Insert(secondTestTicket, e.recordType, e.Index).Return(fmt.Errorf("[ERROR] Attempted to insert a record but got an error from ElasticSearch %v", secondTestTicket))

	err :=	e.InsertTickets(testRecords, e.Index)

	assert.Errorf(t,err,"Error is expected after insertIndex when the Index does exist but elasticsearch service is unavailable, application handles gracefully.")

}
func TestElasticSearchController_SetIndex_WhenIndexDoesntExist(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	mockElasticService.EXPECT().IndexExists("something_other_than_cycle_time").Return(false)

	e.SetIndex("something_other_than_cycle_time")

	assert.Equal(t, "cycle_time", e.Index, "Index should remain the same after SetIndex when Index doesnt exist ")
}

func TestElasticSearchController_SetIndex_WhenIndexExists(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	mockElasticService.EXPECT().IndexExists("something_other_than_cycle_time").Return(true)

	e.SetIndex("something_other_than_cycle_time")

	assert.Equal(t, "something_other_than_cycle_time", e.Index, "Index should be updated after SetIndex when Index does exist ")
}

func TestElasticSearchController_ConnectWithoutConfiguration(t *testing.T) {
	e := ElasticSearchController{
		service:    nil,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	assertPanic(t, func(){
		e.Connect()
	})



}

func TestElasticSearchController_Connect(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{
		service:    mockElasticService,
		Index:      "cycle_time",
		recordType: "cycle_time",
	}

	mockElasticService.EXPECT().NewService().Return(nil)

	err := e.Connect()

	assert.Nil(t, err, "NoError should be thrown when Connect is called with a configuration")
}

func TestElasticSearchController_SetConfiguration(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockElasticService := mock.NewMockElasticSearchServiceInterface(mockCtrl)

	e := ElasticSearchController{}

	e.service = mockElasticService

	e.SetConfiguration("this_specific_domain", true, true)

	assert.NotEqual(t, mockElasticService, e.service, "Service should be replaced with a new configuration object after Setconfig is called properly.")
}

func TestNewElasticSearchController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	comparingElasticObject := services.ElasticSearchService{
		Domain:      "test_domain",
		Sniff:       false,
		HealthCheck: false,
	}
	e := NewElasticSearchController("test_index",comparingElasticObject.Domain,"test_recordType",comparingElasticObject.Sniff,comparingElasticObject.HealthCheck)

	assert.Equal(t,&comparingElasticObject, e.service ,"Objects should be instantiated with the expected details")
	assert.Equal(t,"test_index", e.Index )
	assert.Equal(t, "test_recordType", e.recordType)
}

func assertPanic(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	f()
}