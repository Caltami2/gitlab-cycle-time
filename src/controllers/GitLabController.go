package controllers

import (
	"fmt"
	"github.com/abdallahmaltamimi/go-gitlab"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/structs"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"regexp"
	"strings"
	"time"
)

//GitlabControllerInterface is used for interfacing or mocking during testing
type GitlabControllerInterface interface{
	GetCommitsBetweenTwoTimeStamps(projectID int, firstStamp *time.Time, secondStamp *time.Time) ([]*gitlab.Commit, error)
	GetLastDeployTimeStamp(pid int, ts time.Time) (*time.Time, error)
}

//GitlabController represents a GitLab controller for use during Kafka loops.
type GitlabController struct{
	apiService services.GitLabAPIServiceInterface
	apiToken string
}

//NewGitlabController returns a new GitLab Controller and initializes the apiService in the controller.
func NewGitlabController(apiToken string) *GitlabController {
	apiService := &services.GitLabAPIService{}
	return &GitlabController{
		apiService: apiService,
		apiToken: apiToken,
	}
}




//NewService is used to initialize the official connection to GitLab.
func (g *GitlabController) NewService() error{
	//Notes: NewService assumes apiToken has been set using "service.SetAPIToken()"
	if(g.apiToken == ""){
		err := fmt.Errorf("[ERROR] Couldn't initialize gitlab client due to missing apiToken")
		util.LogErr("Error encountered while initializing service to gitlab api.", err)
		util.Panic("Panic due to error encountered while attempting to connect to GitLab API")
		return err
	}
	g.apiService.SetAPIToken(g.apiToken)
	err := g.apiService.NewService()
	if(err != nil){
		util.LogErr("Error encountered while initializing service to gitlab api.", err)
		util.Panic("Panic due to error encountered while attempting to connect to GitLab API")
		return err
	}
	return nil
}

//GetLastDeployTimeStamp is used to get the last known timestamp in which a project was deployed to production.
func (g *GitlabController) GetLastDeployTimeStamp(pid int, ts time.Time) (*time.Time, error) {
	//Notes: ApiService must be initialized by this point.
	deploys, resp, err := g.apiService.ListProjectDeployments(pid, ts.String(), 100)

	if(err != nil ) {
		err :=  fmt.Errorf("[ERROR] Couldn't get project deployments due to API call failure: %v ", resp.StatusCode)
		util.LogErr("Error encountered while attempting to GetLastDeployTimeStamp", err)
		return nil, err
	}

	lastDeployed := GetTimeStampOfLastDeployToProdFromListOfDeploys(deploys)

	if lastDeployed != nil {
		return lastDeployed, nil
	}

	err =  fmt.Errorf("[Unable to retrieve last deploy timestamp ")
	return nil, err
}


//GetCommitsBetweenTwoTimeStamps is used to get the commits made in between two specific timestamps
func (g *GitlabController) GetCommitsBetweenTwoTimeStamps(projectID int, firstStamp *time.Time, secondStamp *time.Time) ([]*gitlab.Commit, error){
	//Notes: ApiService must be initialized by this point.
	commits, resp, err := g.apiService.ListCommitsBetweenTimeStamps(projectID, firstStamp, secondStamp)

	if err != nil {
		err :=  fmt.Errorf("[ERROR] Couldn't get project deployments due to API call failure: %v ", resp.StatusCode)
		util.LogErr("Error encounterd while attempting to GetCommitsBetweenTwoTimeStamps", err)
		return nil, err
	}

	return commits, nil
}



//GetMergeRequestCommits is used to get the commits correlating to a particular merge request
func (g *GitlabController) GetMergeRequestCommits(projectID int, iid int) ([]*gitlab.Commit, error){
	//Notes: ApiService must be initialized by this point.
	commits, resp, err := g.apiService.GetMergeRequestCommits(projectID, iid)

	if err != nil {
		err :=  fmt.Errorf("[ERROR] Couldn't get project deployments due to API call failure: %v ", resp.StatusCode)
		util.LogErr("Error encounterd while attempting to GetMergeRequestCommits", err)
		return nil, err
	}

	return commits, nil
}



//CheckIfSuccessfulBuildToProd checks if the build or pipeline event are representative of a successful build to prod
func CheckIfSuccessfulBuildToProd(build *structs.Build, pl structs.PipelineEvent) bool{
	//Notes: If the pipeline event is deploying to prod, but build has failed/still in progress, this will return false.
	if build.Stage == "deploy" &&
		build.Status == "success" &&
		(FilterStringForProduction(build.Name) || PipelineEventVariablesContainProductionEnv(pl)){
		return true
	}
	return false
}



//GetTimeStampOfLastDeployToProdFromListOfDeploys gets the earliest known Deployment to production from a list of deploys.
func GetTimeStampOfLastDeployToProdFromListOfDeploys(deploys []*gitlab.Deployment) *time.Time {
	if len(deploys) < 1 {
		return nil
	}
	for i := range deploys{
		if(FilterStringForProduction(deploys[i].Environment.Name) && deploys[i].Deployable.Status == "success") {
			return deploys[i].Deployable.FinishedAt
		}
	}
	return &time.Time{}
}


//FilterStringForProduction returns a boolean value representing whether the passed string (t) represents a production environment or a non-production environment.
func FilterStringForProduction(t string) bool {
	//make all lowercase
	return !strings.Contains(t, "staging") &&
		!strings.Contains(t, "-stage") &&
		!strings.Contains(t, "pre") &&
		(strings.Contains(t, "production") ||
			strings.Contains(t, "prd") ||
			strings.Contains(t, "prod"))
}

//PipelineEventVariablesContainProductionEnv returns a boolean value representing whether the passed PipelineEvent (pl) represents a production environment or a non-production environment.
func PipelineEventVariablesContainProductionEnv(pl structs.PipelineEvent) bool{
	//Notes: Makes use of FilterStringForProduction()
	for i := range pl.ObjectAttributes.Variables{
		if(FilterStringForProduction(pl.ObjectAttributes.Variables[i]["ENV"])){
			return true
		}
	}
	return false
}

//GetJiraTicketsFromAListOfCommits returns an array of strings representing all of the Jira Tickets matching the in-code regex
func GetJiraTicketsFromAListOfCommits(c []*gitlab.Commit) []string {
	tickets := make([]string, 0)
	ticketHolder := make([]string, 0)

	filter := regexp.MustCompile(`([A-Z]{1,10})-?[A-Z]+-\d+`)

	for _, commit := range c {
		ticketHolder = append(ticketHolder, filter.FindAllString(commit.Message, -1)...)
		ticketHolder = append(ticketHolder, filter.FindAllString(commit.Title, -1)...)
	}

	for _, t := range ticketHolder {
		skip := false
		for _, unique := range tickets {
			if t == unique {
				skip = true
				break
			}
		}
		if !skip {
			tickets = append(tickets, t)
		}
	}

	return tickets
}
