package controllers

import (
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
)


//KafkaMessageReactorInterface used for clarity and mocking.
type KafkaMessageReactorInterface interface{
	React(message sarama.ConsumerMessage) error
}

//MessageReactor represents an object intended to react to kafka messages
type MessageReactor struct{
	cycleTimeCalculator CycleTimeControllerInterface
}

//NewMessageReactor Create a MessageReactor object and return a pointer to the created reactor.
func NewMessageReactor(cycleTimeCalculator CycleTimeControllerInterface) *MessageReactor {
	return &MessageReactor{cycleTimeCalculator: cycleTimeCalculator}
}

//React is a function that is called during the Kafka loop. This is the method that reacts to messages.
func (reactor *MessageReactor) React(message sarama.ConsumerMessage) error {
	switch message.Topic {
	case "cdp-gitlab-webhook-merge-requests":
		return reactor.cycleTimeCalculator.HandleMergeRequestMessage(message)
	case "cdp-gitlab-webhook-pipelines":
		return reactor.cycleTimeCalculator.HandlePipelineMessage(message)
	default:
		err := fmt.Errorf("unhandled event for topic %s", message.Topic)
		util.LogErr("Error encountered while attempting to react to the message topic: Topic is unknown/unhandled", err)
		return err
	}
}


