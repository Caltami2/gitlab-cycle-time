package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/abdallahmaltamimi/go-gitlab"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/models"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/structs"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
)

//CycleTimeControllerInterface is used for mocking.
type CycleTimeControllerInterface interface{
	HandlePipelineMessage(message sarama.ConsumerMessage) error
	HandleMergeRequestMessage(message sarama.ConsumerMessage) error
}

//CycleTimeCalculator represents the cycle_time calculator
type CycleTimeCalculator struct {
	ctr               GitlabController
	elasticController ElasticSearchController
}

//NewCycleTimeCalculator returns a new calculator while initializing the internal ElasicSearch
func NewCycleTimeCalculator(ctr GitlabController, elasticController ElasticSearchController) *CycleTimeCalculator {
	return &CycleTimeCalculator{ctr: ctr, elasticController: elasticController}
}

//HandlePipelineMessage controls the high-level logic for actions taken in response to a Pipeline Event in Kafka.
func (c *CycleTimeCalculator) HandlePipelineMessage(message sarama.ConsumerMessage) error {

	var pl structs.PipelineEvent

	err := json.Unmarshal(message.Value, &pl)

	if err != nil {
		util.LogErr("Error encountered while CycleTimeController attempted to unmarshal JSON from the incoming Pipeline message.", err)
		return err
	}

	for _, build := range pl.Builds {
		if CheckIfSuccessfulBuildToProd(&build, pl) ||
			PipelineEventVariablesContainProductionEnv(pl) {
			// Get last time the project deployed to production
			lastDeployTimeStamp, error := c.ctr.GetLastDeployTimeStamp(pl.Project.ID, build.FinishedAt)

			if error != nil {
				util.LogErr("Error encountered while CycleTimeController attempted to GetLastDeployTimeStamp.", err)
				return err
			}

			// Get the commits between the Last time the project was deployed and the timeStamp on the `build` inside the for loop
			deploys, error := c.ctr.GetCommitsBetweenTwoTimeStamps(pl.Project.ID, lastDeployTimeStamp, &build.FinishedAt)

			if error != nil {
				util.LogErr("Error encountered while CycleTimeController attempted to GetCommitsBetweenTwoTimeStamps.", err)
				return err
			}

			tickets := models.ConstructTickets(GetJiraTicketsFromAListOfCommits(deploys), &build.FinishedAt, ".stopped")

			err := c.elasticController.InsertTickets(tickets,c.elasticController.Index)

			if err != nil {
				util.LogErr("Error encountered while CycleTimeController attempted to insert tickets into elasticsearch.", err)
				return err
			}

		}

	}
	return nil
}

//HandleMergeRequestMessage controls the high-level logic for actions taken in response to a Merge Request in Kafka.
func (c *CycleTimeCalculator) HandleMergeRequestMessage(message sarama.ConsumerMessage) error {
	var mr structs.MergeRequestWebhook

	err := json.Unmarshal(message.Value, &mr)

	if err != nil {
		util.LogErr("Error encountered while CycleTimeController attempted to unmarshal JSON from the incoming Merge Request message.", err)
		return err
	}
	var commits []*gitlab.Commit
	commits, err = c.ctr.GetMergeRequestCommits(mr.Project.ID, int(mr.ObjectAttributes.IID))


	if err != nil {
		util.LogErr("Error encountered while CycleTimeController attempted to retrieve commits from the incoming Merge Request message.", err)
		return err
	}

	if len(commits) <= 0 {
		err = fmt.Errorf("No commits found")
		util.LogErr("Error encountered while CycleTimeController attempted to retrieve commits from the incoming Merge Request message.", err)
		return  err
	}

	tickets := GetJiraTicketsFromAListOfCommits(commits)
	timestamp := commits[len(commits)-1].AuthoredDate
	constructedTickets := models.ConstructTickets(tickets,timestamp, ".started")

	err = c.elasticController.InsertTickets(constructedTickets,c.elasticController.Index)



	if err != nil {
		util.LogErr("Error encountered while CycleTimeController attempted to insert tickets into elasticsearch.", err)
		return err
	}
	return nil
}
