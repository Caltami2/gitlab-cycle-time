package controllers

import (
	"fmt"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/models"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
)

//ElasticSearchController initializes elasticsearch and interact with the service layer.
type ElasticSearchController struct{
	//NOTES : ElasticSearchController requires you to use the Connect function before attempting to access the client.
	service services.ElasticSearchServiceInterface
	Index   string
	recordType string
}

//NewElasticSearchController returns a new controller and initializes the internal config details.
func NewElasticSearchController(index string, domain string, recordType string, sniff bool, healthcheck bool) *ElasticSearchController {
	//NOTES : ElasticSearchController requires you to use the Connect function before attempting to access the client.
	elasticService := services.ElasticSearchService{
		Domain:      domain,
		Sniff:       sniff,
		HealthCheck: healthcheck,
	}
	newElasticController :=  &ElasticSearchController{service: &elasticService, Index: index, recordType: recordType}
	util.LogInfo(fmt.Sprintf("ElasticSearchController created Domain: %v, Index: %v, Sniff: %v, Healthcheck: %v,", elasticService.Domain, newElasticController.Index, elasticService.Sniff, elasticService.HealthCheck))
	return newElasticController
}

//SetConfiguration replaces the current ElasticSearch Configuration with the specified details.
func (e *ElasticSearchController) SetConfiguration(domain string, sniff bool, healthCheck bool) {
	//NOTES : Set config uses the remaining details from "NewElasticSearchController" to initialize the config.

	service := services.NewElasticSearchService(domain,sniff,healthCheck)
	e.service = &service
	util.LogInfo(fmt.Sprintf(" ElasticSearchController configuration updated Domain: %v, Index: %v, Sniff: %v, Healthcheck: %v,", domain, e.Index, sniff,  healthCheck))
}

//Connect is used to initialize the official connection to ElasticSearch.
func (e *ElasticSearchController) Connect() error {
	//Notes: If this is not called before attempting to use InsertTicket, CreateIndex or InsertTickets, a nil pointer error will result and panic.
	if e.service == nil{
		err := fmt.Errorf("[ERROR] Attempted to connect, but configuration was invalid or empty: %v", e.service)
		util.LogErr("Error encountered while attempting to connect to ElasticSearch", err)
		util.Panic("Panic due to ElasticSearch being unavailable or inaccessible.")
		return err
	}
	return e.service.NewService()
}

//InsertTicket is used to Insert Ticket into the specified string index.
func (e *ElasticSearchController) InsertTicket(record interface{}, index string) error {
	//Notes: InsertTicket takes an interface as the record. However, it requires an interface capable of converting to a string.
	//Notes: Index must exist before using InsertTickets or it will return an error.
	if !e.service.IndexExists(index){
		err :=  fmt.Errorf("[ERROR] Attempted to insert a record, but index doesn't exist. Index: %v", index)
		util.LogErr("Error encountered while attempting to insert a record into to ElasticSearch", err)
		return err
	}
	return e.service.Insert(record,e.recordType,index)
}

//InsertTickets (plural) uses the above method to insert tickets
func (e *ElasticSearchController) InsertTickets(records []models.Ticket, index string) error {
	//Notes: InsertTickets takes a model.Ticket as the record while InsertTicket takes in a record.
	//Notes: Index must exist before using InsertTickets or it will return an error.
	if !e.service.IndexExists(index){
		err := fmt.Errorf("[ERROR] Attempted to insert a record, but index doesn't exist. Index: %v", index)
		util.LogErr("Error encountered while attempting to Insert a series of tickets into elasticsearch", err)
		return err
	}
	for record := range records {
		err := e.service.Insert(records[record],e.recordType,index)
		if err != nil {
			util.LogErr("Error encountered while attempting to Insert a series of tickets into elasticsearch", err)
			return err
		}
	}
	return nil
}

//CreateIndex creates an index in the elasticsearch client for records per mapping.
func (e *ElasticSearchController) CreateIndex(index string, mapping string) error {
	//Notes: Index must not already exist or it will return an error. Use IndexExists to check if index does exist before calling.
	if e.service.IndexExists(index) {
		err:= fmt.Errorf("[ERROR] Attempted to create the index, but index already exists. Index: %v", index)
		util.LogErr("Error encountered while attempting to create an index in elasticsearch.", err)
		return err
	}
	return e.service.CreateIndex(index,mapping)
}


//SetIndex sets the index in the elasticsearch client for records.
func (e *ElasticSearchController) SetIndex(index string) error {
	//Notes: Index must not already exist or it will return an error. Use IndexExists to check if index does exist before calling.
	if !e.service.IndexExists(index){
		err := fmt.Errorf("[ERROR] Attempted to set the index, but index doesn't exist. Index: %v", index)
		util.LogErr("Error encountered while attempting to set the index.", err)
		return err
	}
	e.Index = index
	return nil
}