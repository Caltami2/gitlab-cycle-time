package controllers

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/services"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
)

//KafkaController struct represents a Kafka controller
type KafkaController struct {
	config  sarama.Config
	domain string
	verbose bool
	brokers string
	group   string
	topics  string
	reactor KafkaMessageReactorInterface
	consumerService services.KafkaConsumerService
}

//InitializeConfiguration initializes Configuration for Kafka Consumer using provided details.
func (k *KafkaController) InitializeConfiguration(brokers string, topics string, group string, version string, assignor string, oldest bool)  error{
	//Notes: Config is not verified during this step, use "initializeConsumer()" to ensure consumer can connect.
	if brokers == "" ||
		topics == "" ||
		group == "" {
		err := fmt.Errorf("[ERROR] Brokers, topics, or group value missing. Brokers: %v , Topics: %v, Group: %v", brokers, topics, group)
		util.LogErr("Error encountered while attempting to initialize configuration.", err)
		return err
		}
	k.brokers = brokers
	k.topics = topics
	k.group = group

	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	kafkaVersion, err := sarama.ParseKafkaVersion(version)
	if err != nil {
		err := fmt.Errorf("[ERROR] parsing Kafka version: %v", err)
		util.LogErr("Error while parsing kafka version. " , err)
		return err
	}

	config := sarama.NewConfig()

	config.Version = kafkaVersion

	switch assignor {
	case "sticky":
		config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategySticky
	case "roundrobin":
		config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	case "range":
		config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
	default:
		err := fmt.Errorf("[ERROR] Unrecognized consumer group partition assignor: %s", assignor)
		util.LogErr("Error while creating kafka config.", err)

		return err
	}

	if oldest {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}
	k.config = *config
	return nil
}

//SetReactor sets the Reactor to an Already Initialized Reactor.
func (k *KafkaController) SetReactor(reactor KafkaMessageReactorInterface){

	//Notes: Method can possibly take in an invalid reactor due to the Interface, ensure input is valid before calling SetRector.
	k.reactor = reactor
}

//SetVerbosity sets the Verbosity flag to a KafkaService.
func (k *KafkaController) SetVerbosity(verbose bool) {
	//Notes: This should be called BEFORE StartConsumerGroup() or initializeConsumer()
	if verbose {
		sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	}
	k.verbose = true
}

//StartConsumerGroup runs a consumerGroup for the kafka loop.
func (k *KafkaController) StartConsumerGroup() sarama.ConsumerGroup {
	//Notes: This is the main loop that keeps the application running. This method listens for a close os.Signal to Exit
	//This defines the Context type, which carries deadlines, cancellation signals, and other request-scoped values across API boundaries and between processes.
	ctx, cancel := context.WithCancel(context.Background())

	wg := sync.WaitGroup{}

	k.initializeConsumer()

	k.consumerService.SetReactor(k.reactor)

	consumerGroupClient, err := k.consumerService.NewConsumerGroup(k.brokers, k.group, k.config)

	if err != nil {
		util.LogErr("Error encountered while starting consumer group", err)
		util.Panic("Panic during consumption failure. Kafka unreachable or configuration issues exist.")
	}

	wg.Add(1)
	go func() {
		defer wg.Done()

		for {
			k.kafkaLoop(ctx,consumerGroupClient)
		}
	}()

	<-*k.consumerService.GetReady() // Await till the consumer has been set up

	util.LogInfo("Sarama consumer up and running!...")

	k.waitForClose(ctx, cancel, wg, consumerGroupClient)

	return nil
}

//initializeConsumer sets the consumer as "Ready" by initializing the service and the ready channel
func (k *KafkaController) initializeConsumer(){
	k.consumerService = services.KafkaConsumerService{
	}
	k.consumerService.SetReady(make(chan bool))
}

//kafkaLoop is called throughout every loop to consume from the Client.
func (k *KafkaController) kafkaLoop(ctx context.Context, consumerGroupClient sarama.ConsumerGroup){

	//Notes: After each loop of claims, the method verifies that the consumer is Ready. If not it will await until it is.
	if err := consumerGroupClient.Consume(ctx, strings.Split(k.topics, ","), &k.consumerService); err != nil {
		util.Panic(fmt.Sprintf("Panicing due to error from consumer service. This could mean that kafka is unreachable or there is an error with configuration or the consumption loop: %v", err))
	}

	// check if context was cancelled, signaling that the consumer should stop
	if ctx.Err() != nil {
		util.LogErr("Context cancelled, consumer stopping", ctx.Err())
		return
	}

	k.consumerService.SetReady(make(chan bool))
}

//waitForClose is called at the end of SetUpConsumer
func (k *KafkaController) waitForClose(ctx context.Context, cancel func(), wg sync.WaitGroup, group sarama.ConsumerGroup){

	//Notes: After each loop of claims, the method verifies that the consumer is Ready. If not it will await until it is.
	sigterm := make(chan os.Signal, 1)

	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-ctx.Done():
		util.LogInfo("terminating: context cancelled")
	case <-sigterm:
		util.LogInfo("terminating: via signal")
	}


	cancel()
	wg.Wait()

	if err := group.Close(); err != nil {
		util.LogErr("Error encountered while attempting to close the client.", err)
		util.Panic("Error closing client, panic.")
	}

}
