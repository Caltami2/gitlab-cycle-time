package structs

import "time"

//PipelineEvent struct
type PipelineEvent struct {
	ObjectKind       string                   `json:"object_kind"`
	User             user                     `json:"user"`
	Project          project                  `json:"project"`
	Commit           commit                   `json:"commit"`
	ObjectAttributes PipelineObjectAttributes `json:"object_attributes"`
	MergeRequest     mergeRequest             `json:"merge_request"`
	Builds           []Build                 `json:"builds"`
}

//PipelineObjectAttributes struct
type PipelineObjectAttributes struct {
	ID             int                 `json:"id"`
	IID            int                 `json:"iid"`
	Ref            string              `json:"ref"`
	Tag            bool                `json:"tag"`
	Sha            string              `json:"sha"`
	BeforeSha      string              `json:"before_sha"`
	Source         string              `json:"source"`
	Status         string              `json:"status"`
	DetailedStatus string              `json:"detailed_status"`
	Stages         []string            `json:"stages"`
	CreatedAt      time.Time           `json:"created_at"`
	FinishedAt     time.Time           `json:"finished_at"`
	Duration       int                 `json:"duration"`
	Variables      []map[string]string `json:"variables"`
}

//Build struct
type Build struct {
	ID            int           `json:"id"`
	Stage         string        `json:"stage"`
	Name          string        `json:"name"`
	Status        string        `json:"status"`
	CreatedAt     time.Time     `json:"created_at"`
	StartedAt     time.Time     `json:"started_at"`
	FinishedAt    time.Time     `json:"finished_at"`
	When          string        `json:"when"`
	Manual        bool          `json:"manual"`
	User          user          `json:"user"`
	Runner        runner        `json:"runner"`
	Artifactsfile artifactsfile `json:"artifactsfile"`
}
type source struct {
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebURL            string `json:"web_url"`
	AvatarURL         string `json:"avatar_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	GitHTTPURL        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	Homepage          string `json:"homepage"`
	URL               string `json:"url"`
	SSHURL            string `json:"ssh_url"`
	HTTPURL           string `json:"http_url"`
}
type target struct {
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebURL            string `json:"web_url"`
	AvatarURL         string `json:"avatar_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	GitHTTPURL        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	Homepage          string `json:"homepage"`
	URL               string `json:"url"`
	SSHURL            string `json:"ssh_url"`
	HTTPURL           string `json:"http_url"`
}
type lastCommit struct {
	ID        string    `json:"id"`
	Message   string    `json:"message"`
	Timestamp time.Time `json:"timestamp"`
	URL       string    `json:"url"`
	Author    author    `json:"author"`
}
type assignee struct {
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
}
type mergeRequest struct {
	ID              int        `json:"id"`
	TargetBranch    string     `json:"target_branch"`
	SourceBranch    string     `json:"source_branch"`
	SourceProjectID int        `json:"source_project_id"`
	AssigneeID      int        `json:"assignee_id"`
	AuthorID        int        `json:"author_id"`
	Title           string     `json:"title"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	MilestoneID     int        `json:"milestone_id"`
	State           string     `json:"state"`
	MergeStatus     string     `json:"merge_status"`
	TargetProjectID int        `json:"target_project_id"`
	IID             int        `json:"iid"`
	Description     string     `json:"description"`
	Position        int        `json:"position"`
	LockedAt        time.Time  `json:"locked_at"`
	Source          source     `json:"source"`
	Target          target     `json:"target"`
	LastCommit      lastCommit `json:"last_commit"`
	WorkInProgress  bool       `json:"work_in_progress"`
	Assignee        assignee   `json:"assignee"`
	URL             string     `json:"url"`
}
type runner struct {
	ID          int    `json:"id"`
	Description string `json:"description"`
	Active      bool   `json:"active"`
	IsShared    bool   `json:"is_shared"`
}
type artifactsfile struct {
	Filename string `json:"filename"`
	Size     string `json:"size"`
}
type user struct {
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
}
type project struct {
	ID                int    `json:"id"`
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebURL            string `json:"web_url"`
	AvatarURL         string `json:"avatar_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	GitHTTPURL        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	Homepage          string `json:"homepage"`
	URL               string `json:"url"`
	SSHURL            string `json:"ssh_url"`
	HTTPURL           string `json:"http_url"`
	CiConfigPath      string `json:"ci_config_path"`
}
type author struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}
type commit struct {
	AuthorEmail    string   `json:"author_email"`
	AuthorName     string   `json:"author_name"`
	AuthoredDate   string   `json:"authored_date"`
	CommittedDate  string   `json:"committed_date"`
	CommitterEmail string   `json:"committer_email"`
	CommitterName  string   `json:"committer_name"`
	CreatedAt      string   `json:"created_at"`
	ID             string   `json:"id"`
	Message        string   `json:"message"`
	ParentIds      []string `json:"parent_ids"`
	ShortID        string   `json:"short_id"`
	Title          string   `json:"title"`
}