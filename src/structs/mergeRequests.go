package structs

import "time"

//MergeRequestWebhook struct
type MergeRequestWebhook struct {
	ObjectKind       string             `json:"object_kind"`
	User             user               `json:"user"`
	Project          project            `json:"project"`
	Repository       repository         `json:"repository"`
	ObjectAttributes mRObjectAttributes `json:"object_attributes"`
	Labels           []labels           `json:"labels"`
	Changes          changes            `json:"changes"`
}

type mRObjectAttributes struct {
	ID             int                 `json:"id"`
	IID            int                 `json:"iid"`
	Ref            string              `json:"ref"`
	Tag            bool                `json:"tag"`
	Sha            string              `json:"sha"`
	BeforeSha      string              `json:"before_sha"`
	Source         source              `json:"source"`
	Status         string              `json:"status"`
	DetailedStatus string              `json:"detailed_status"`
	Stages         []string            `json:"stages"`
	CreatedAt      time.Time           `json:"created_at"`
	FinishedAt     time.Time           `json:"finished_at"`
	Duration       int                 `json:"duration"`
	Variables      []map[string]string `json:"variables"`
	TargetBranch   string              `json:"target_branch"`
	SourceBranch   string              `json:"source_branch"`
}

type repository struct {
	Name        string `json:"name"`
	URL         string `json:"url"`
	Description string `json:"description"`
	Homepage    string `json:"homepage"`
}
type updatedByID struct {
	Previous interface{} `json:"previous"`
	Current  int         `json:"current"`
}
type updatedAt struct {
	Previous string `json:"previous"`
	Current  string `json:"current"`
}
type previous struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Color       string    `json:"color"`
	ProjectID   int       `json:"project_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Template    bool      `json:"template"`
	Description string    `json:"description"`
	Type        string    `json:"type"`
	GroupID     int       `json:"group_id"`
}
type current struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Color       string    `json:"color"`
	ProjectID   int       `json:"project_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Template    bool      `json:"template"`
	Description string    `json:"description"`
	Type        string    `json:"type"`
	GroupID     int       `json:"group_id"`
}
type labels struct {
	Previous []previous `json:"previous"`
	Current  []current  `json:"current"`
}
type changes struct {
	UpdatedByID updatedByID `json:"updated_by_id"`
	UpdatedAt   updatedAt   `json:"updated_at"`
	Labels      labels      `json:"labels"`
}
