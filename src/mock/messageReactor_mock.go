// Code generated by MockGen. DO NOT EDIT.
// Source: messageReactor.go

// Package controllers is a generated GoMock package.
package mock

import (
	sarama "github.com/Shopify/sarama"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockKafkaMessageReactorInterface is a mock of KafkaMessageReactorInterface interface
type MockKafkaMessageReactorInterface struct {
	ctrl     *gomock.Controller
	recorder *MockKafkaMessageReactorInterfaceMockRecorder
}

// MockKafkaMessageReactorInterfaceMockRecorder is the mock recorder for MockKafkaMessageReactorInterface
type MockKafkaMessageReactorInterfaceMockRecorder struct {
	mock *MockKafkaMessageReactorInterface
}

// NewMockKafkaMessageReactorInterface creates a new mock instance
func NewMockKafkaMessageReactorInterface(ctrl *gomock.Controller) *MockKafkaMessageReactorInterface {
	mock := &MockKafkaMessageReactorInterface{ctrl: ctrl}
	mock.recorder = &MockKafkaMessageReactorInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockKafkaMessageReactorInterface) EXPECT() *MockKafkaMessageReactorInterfaceMockRecorder {
	return m.recorder
}

// React mocks base method
func (m *MockKafkaMessageReactorInterface) React(message sarama.ConsumerMessage) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "React", message)
	ret0, _ := ret[0].(error)
	return ret0
}

// React indicates an expected call of React
func (mr *MockKafkaMessageReactorInterfaceMockRecorder) React(message interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "React", reflect.TypeOf((*MockKafkaMessageReactorInterface)(nil).React), message)
}
