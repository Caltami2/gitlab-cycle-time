package services

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/mock"
	"net"
	"testing"
)

func TestKafkaConsumerService_Setup(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}

	mockConsumerGroupSession := mock.NewMockConsumerGroupSession(mockCtrl)

	err := consumer.Setup(mockConsumerGroupSession)

	assert.NoErrorf(t,err,"No errr should be returned. Setup should be implemented from sarama interface and should never return errors as they would not be captured. ")
}

func TestKafkaConsumerService_Cleanup(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}

	mockConsumerGroupSession := mock.NewMockConsumerGroupSession(mockCtrl)

	err := consumer.Cleanup(mockConsumerGroupSession)

	assert.NoErrorf(t,err,"No errr should be returned. Cleanup should be implemented from sarama interface and should never return errors as they would not be captured. ")
}

func TestKafkaConsumerService_ConsumeClaim(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockReactor := mock.NewMockKafkaMessageReactorInterface(mockCtrl)

	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: mockReactor,
	}

	mockConsumerGroupSession := mock.NewMockConsumerGroupSession(mockCtrl)

	mockConsumerGroupClaim := mock.NewMockConsumerGroupClaim(mockCtrl)

	mockMessage := &sarama.ConsumerMessage{}

	mockChannel := make(chan *sarama.ConsumerMessage)

	go func() {
		mockChannel <- mockMessage
		close(mockChannel)
	}()

	mockConsumerGroupClaim.EXPECT().Messages().Return(mockChannel)

	mockConsumerGroupSession.EXPECT().MarkMessage(mockMessage,"")

	mockReactor.EXPECT().React(*mockMessage).Return(nil)

	err := consumer.ConsumeClaim(mockConsumerGroupSession,mockConsumerGroupClaim)

	assert.NoErrorf(t,err,"No errr should be returned. Setup should be implemented from sarama interface and should never return errors as they would not be captured. ")
}

func TestKafkaConsumerService_ConsumeClaim_Error(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockReactor := mock.NewMockKafkaMessageReactorInterface(mockCtrl)

	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: mockReactor,
	}

	mockConsumerGroupSession := mock.NewMockConsumerGroupSession(mockCtrl)

	mockConsumerGroupClaim := mock.NewMockConsumerGroupClaim(mockCtrl)

	mockMessage := &sarama.ConsumerMessage{}

	mockChannel := make(chan *sarama.ConsumerMessage)

	go func() {
		mockChannel <- mockMessage
		close(mockChannel)
	}()

	mockConsumerGroupClaim.EXPECT().Messages().Return(mockChannel)
	mockReactor.EXPECT().React(*mockMessage).Return(fmt.Errorf("A fake error"))
	err := consumer.ConsumeClaim(mockConsumerGroupSession,mockConsumerGroupClaim)

	assert.NoErrorf(t,err,"No errr should be returned. ConsumeClaim should be implemented from sarama interface and should never return errors as they would not be captured. ")
}

func TestKafkaConsumerService_SetReactor(t *testing.T) {
	wantErr := true

	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}

	if err := consumer.SetReactor(nil); (err != nil) != wantErr {
		t.Errorf("SetReactor() error = %v, wantErr %v", err, wantErr)
	}

	//
	wantErr = false

	consumer = &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockReactor := mock.NewMockKafkaMessageReactorInterface(mockCtrl)

	if err := consumer.SetReactor(mockReactor); (err != nil) != wantErr {
		t.Errorf("SetReactor() error = %v, wantErr %v", err, wantErr)
	}
}

func TestKafkaConsumerService_NewConsumerGroup_failure(t *testing.T) {
	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}
	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	kafkaVersion, err := sarama.ParseKafkaVersion("2.1.1")

	config := sarama.NewConfig()

	config.Version = kafkaVersion


	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategySticky

	config.Consumer.Offsets.Initial = sarama.OffsetOldest

	_,err = consumer.NewConsumerGroup("fail","unkown", *config)

	assert.Errorf(t,err,"Error should be returned for bad domains")
}

func TestKafkaConsumerService_NewConsumerGroup_success(t *testing.T) {
	consumer := &KafkaConsumerService{
		ready:   make(chan bool),
		reactor: nil,
	}
	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	kafkaVersion, err := sarama.ParseKafkaVersion("2.1.1")

	config := sarama.NewConfig()

	config.Version = kafkaVersion


	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategySticky

	config.Consumer.Offsets.Initial = sarama.OffsetOldest

	txtrecords, err := net.LookupTXT("metrics-kafka-brokers.cdp-npe.t-mobile.com")

	_,err = consumer.NewConsumerGroup(txtrecords[0],"unkown", *config)

	assert.NoErrorf(t,err,"No Error should be returned for valid domain and configuration")
}