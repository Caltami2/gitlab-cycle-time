package services

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/models"
	"testing"
	"time"
)

var elasticSearchTestDomain string

func TestElasticSearchService_IndexExists_No(t *testing.T) {

	e := NewElasticSearchService("http://elasticsearch:9200",false,false)

	e.NewService()

	result := e.IndexExists("cycle_time")

	assert.Equal(t, result, false, "Index Exists should return false if index doesn't exist")

}


func TestElasticSearchService_CreateIndex(t *testing.T) {

	e := NewElasticSearchService("http://elasticsearch:9200",false,false)

	e.NewService()

	mapping := `{
				"mappings":{
				"cycle_time":{
					"properties":{
						"time_stamp":{
							"type":"date"
						},
						"ticket":{
							"type":"keyword"
						}
					}
				}
			}
		}`

	e.client.DeleteIndex("cycle_time")

	err := e.CreateIndex("cycle_time", mapping)

	assert.NoErrorf(t, err, "Cycle_time index insert should not return an error if index doesnt exist.")

	err = e.CreateIndex("cycle_time", mapping)

	assert.Errorf(t, err, "Cycle_time index insert should return an error if index does exist.")

}

func TestElasticSearchService_IndexExists_yes(t *testing.T) {

	e := NewElasticSearchService("http://elasticsearch:9200",false,false)

	e.NewService()

	result := e.IndexExists("cycle_time")

	assert.Equal(t, result, true, "Index Exists should return true if index does exist")

}

func TestElasticSearchService_Insert(t *testing.T) {

	e := NewElasticSearchService("http://elasticsearch:9200",true,false)

	e.NewService()

	mapping := `{
				"mappings":{
				"cycle_time":{
					"properties":{
						"time_stamp":{
							"type":"date"
						},
						"ticket":{
							"type":"keyword"
						}
					}
				}
			}
		}`


	e.CreateIndex("cycle_time", mapping)



	dummyTicket := models.Ticket{
		TicketName: "",
		TimeStamp:  &time.Time{},
	}

	err := e.Insert(dummyTicket,"cycle_time","cycle_time")

	assert.NoErrorf(t, err, "No Error Should be thrown if insert is called when the index does exist")


}

func TestElasticSearchService_NewService(t *testing.T) {
	e := ElasticSearchService{
		Domain:      "test",
		Sniff:       false,
		HealthCheck: false,

	}


	e.NewService()

}

func TestNewElasticSearchService(t *testing.T) {
	domain := "testDomain"
	sniff := false
	healthCheck := false

	elasticService := NewElasticSearchService(domain,sniff,healthCheck)

	assert.Equal(t, domain, elasticService.Domain, "Domain should be set properly.")
	assert.Equal(t, sniff, elasticService.Sniff, "Sniff should be set properly.")
	assert.Equal(t, healthCheck, elasticService.HealthCheck, "HealthCheck should be set properly.")

}
