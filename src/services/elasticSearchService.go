package services

import (
	"context"
	"github.com/olivere/elastic/v7"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
)

//ElasticSearchServiceInterface interface
type ElasticSearchServiceInterface interface {
	//Use:
	//Notes:
	NewService() error
	Insert(record interface{}, recordType string, index string) error
	CreateIndex(indexName string, mapping string) error
	IndexExists(indexName string)  bool
}

//ElasticSearchService struct
type ElasticSearchService struct {
	//Use:
	//Notes:
	Domain string
	Sniff bool
	HealthCheck bool
	client *elastic.Client
}

//NewElasticSearchService func
func NewElasticSearchService(domain string, sniff bool, healthCheck bool) ElasticSearchService {
	//Use:
	//Notes:
	newElasticSearchServiceObject := &ElasticSearchService{
		Domain: domain,
		Sniff: sniff,
		HealthCheck: healthCheck,
	}
	util.LogInfo("Elasticsearch service initialized")
	return *newElasticSearchServiceObject
}

//NewService func
func (e *ElasticSearchService) NewService() error {
	client, err := elastic.NewClient(
		elastic.SetURL(e.Domain),
		elastic.SetSniff(e.Sniff),
		elastic.SetHealthcheck(e.HealthCheck),
	)
	if err != nil {
		util.LogErr("Error hit while setting up ElasticSearch Client", err)
		return err
	}
	e.client = client

	return nil
}

//Insert func
func (e *ElasticSearchService) Insert(record interface{}, recordType string, index string) error {
	ctx := context.Background()
	_, err := e.client.Index().Index(index).Type(recordType).BodyJson(record).Do(ctx)
	if err != nil {
		util.LogErr("Error hit while inserting record into Elasticsearch", err)
		return err
	}
	return nil
}

//CreateIndex creates a new index using the string and mapping provided.
func (e *ElasticSearchService) CreateIndex(indexName string, mapping string) error {
	//Use:
	//Notes:
	ctx := context.Background()

	_, err := e.client.CreateIndex(indexName).BodyString(mapping).Do(ctx)
	if err != nil {
		util.LogErr("Error hit while creating index in Elasticsearch", err)
		return err
	}
	return nil
}

//IndexExists func
func (e *ElasticSearchService) IndexExists(indexName string) bool {
	//Use:
	//Notes:
	exists, err := e.client.IndexExists(indexName).Do(context.Background())
	if err != nil {
		util.LogErr("Error hit while checking if index cycle_time exists; Defaulting to false. Error: %v", err)
		return false
	}
	return exists
}
