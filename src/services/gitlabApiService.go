package services

import (
	"fmt"
	gitlab "github.com/abdallahmaltamimi/go-gitlab"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"log"
	"net/http"
	"time"
)

//GitLabAPIServiceInterface interface
type GitLabAPIServiceInterface interface {

	NewService() error
	GetMergeRequestCommits( pid interface{},iid int) ([]*gitlab.Commit, *gitlab.Response, error)
	ListProjectDeployments(pid interface{}, updatedBefore string, perPage int)  ([]*gitlab.Deployment, *gitlab.Response, error)
	ListCommitsBetweenTimeStamps( pid interface{}, firstTimeStamp, secondTimeStamp *time.Time)  ([]*gitlab.Commit, *gitlab.Response, error)
	SetAPIToken(token string)
}

//GitLabAPIService struct
type GitLabAPIService struct {
	//Use:
	//Notes:
	Client *gitlab.Client
	apiToken string
}

//SetAPIToken func
func (g *GitLabAPIService) SetAPIToken(token string){
	//Use:
	//Notes:
	g.apiToken = token

}

//NewService func
func (g *GitLabAPIService) NewService() error {
	//Use:
	//Notes:
	if g.apiToken == "" {
		err := fmt.Errorf("[ERROR] Gitlab API token was not set")
		util.LogErr("Gitlab API token was not set", err )
		return err
	}
	g.Client = gitlab.NewClient(&http.Client{},g.apiToken)
	return nil
}

//GetMergeRequestCommits func
func (g *GitLabAPIService) GetMergeRequestCommits(pid interface{},iid int) ([]*gitlab.Commit, *gitlab.Response, error) {
	//Use:
	//Notes:
	commits, resp, err := g.Client.MergeRequests.GetMergeRequestCommits(pid, iid, &gitlab.GetMergeRequestCommitsOptions{})
	if err != nil {
		util.LogErr("Error encountered while attempting to Get Merge Request commits from Gitlab API", err )
	}
	return commits,resp,err
}

//ListProjectDeployments func
func (g *GitLabAPIService) ListProjectDeployments(pid interface{}, updatedBefore string, perPage int) ([]*gitlab.Deployment, *gitlab.Response, error)  {
	//Use:
	//Notes:
	order := "updated_at"
	sort := "desc"
	before := updatedBefore

	deploys, resp, err := g.Client.Deployments.ListProjectDeployments(pid,
		&gitlab.ListProjectDeploymentsOptions{
			OrderBy:       &order,
			Sort:          &sort,
			UpdatedBefore: &before,
			ListOptions: gitlab.ListOptions{
				PerPage: perPage,
			},
		},
	)
	if err != nil {
		util.LogErr("Error encountered while attempting to ListProjectDeployments from Gitlab API", err )
	}
	return deploys,resp,err
}

//ListCommitsBetweenTimeStamps func
func (g *GitLabAPIService) ListCommitsBetweenTimeStamps( pid interface{}, firstTimeStamp *time.Time, secondTimeStamp *time.Time)  ([]*gitlab.Commit, *gitlab.Response, error) {
	//Use:
	//Notes:

	tOneStr := firstTimeStamp.Format(time.RFC3339)
	tTwoStr := secondTimeStamp.Format(time.RFC3339)

	tOneTime, err := time.Parse(time.RFC3339, tOneStr)

	if err != nil {
		log.Println(err)
		return nil,nil, err
	}

	tTwoTime, err := time.Parse(time.RFC3339, tTwoStr)
	if err != nil {
		log.Println(err)
		return nil,nil, err
	}



	commits, resp, err := g.Client.Commits.ListCommits(pid, &gitlab.ListCommitsOptions{
		Since: gitlab.Time(tOneTime),
		Until: gitlab.Time(tTwoTime),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
		},
	})

	if err != nil {
		util.LogErr("Error encountered while attempting to ListCommitsBetweenTimeStamps from Gitlab API", err )
	}

	return commits, resp, err
}