package services

import (
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"strings"
)

//KafkaMessageReactorInterface interface
type KafkaMessageReactorInterface interface{
	React(message sarama.ConsumerMessage) error
}

//KafkaConsumerServiceInterface interface
type KafkaConsumerServiceInterface interface {
	//Use:
	//Notes:
	SetReactor(r KafkaMessageReactorInterface) error
	ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error
	Cleanup(session sarama.ConsumerGroupSession) error
	Setup(session sarama.ConsumerGroupSession) error
	NewConsumerGroup(brokers string, group string, config sarama.Config) (sarama.ConsumerGroup, error)
}

//KafkaConsumerService struct
type KafkaConsumerService struct{
	//Use:
	//Notes:
	ready   chan bool
	reactor KafkaMessageReactorInterface
}

//SetReactor func
func (consumer *KafkaConsumerService) SetReactor(r KafkaMessageReactorInterface) error{
	//Use:
	//Notes:
	if r == nil{
		err := fmt.Errorf("Reactor not intialized or equal to nil")
		util.LogErr("Error encountered while attempting to SetReactor", err)
		return err
	}
	consumer.reactor = r
	return nil
}

//SetReady func
func (consumer *KafkaConsumerService) SetReady(ready chan bool) {
	consumer.ready = ready
	util.LogInfo("Consumer 'ready' has been set.")
}

//GetReady func
func (consumer *KafkaConsumerService) GetReady() *(chan bool) {
	//Use:
	//Notes:
	return &consumer.ready
}

//Setup func
func (consumer *KafkaConsumerService) Setup(sarama.ConsumerGroupSession) error {
	//Use:
	//Notes:
	close(consumer.ready)
	return nil
}

//Cleanup func
func (consumer *KafkaConsumerService) Cleanup(sarama.ConsumerGroupSession) error {
	//Use:
	//Notes:
	// Implement any cleanup steps here.
	return nil
}

//ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *KafkaConsumerService) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	//Use:
	//Notes:
	messages := claim.Messages()

	for message := range messages {

		err := consumer.reactor.React(*message)

		if err != nil {
			util.LogErr("Message was unable to be marked as consumed", err)
		} else {
			session.MarkMessage(message, "")
		}

	}
	return nil
}

//NewConsumerGroup func
func (consumer *KafkaConsumerService) NewConsumerGroup(brokers string, group string, config sarama.Config) (sarama.ConsumerGroup, error) {
	//Use:
	//Notes:
	consumerGroup, err := sarama.NewConsumerGroup(strings.Split(brokers, ","), group, &config)
	if err != nil {
		util.LogErr("Error encountered while attempting to intiailize a new consumer group." ,err)
		return nil, err
	}
	return consumerGroup, nil
}
