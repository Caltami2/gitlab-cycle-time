package services

import (
	"github.com/abdallahmaltamimi/go-gitlab"
	"github.com/stretchr/testify/assert"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func TestGitLabAPIService_GetMergeRequestCommits(t *testing.T) {

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/merge_requests/123/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_merge_request_commits.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	commits, resp, err := c.GetMergeRequestCommits("12345", 123)

	assert.NoErrorf(t,err,"Asserting there is no error returned from Merge Request Commits")
	assert.Equal(t , "200 OK" , resp.Status, "Assert Response is 200")
	assert.Equal(t, "6104942438c14ec7bd21c6cd5bd995272b3faff6", commits[0].ID, "Asserting order is maintained.")
	log.Print(err)
}

func TestGitLabAPIService_GetMergeRequestCommits_Failure(t *testing.T) {

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/merge_requests/123/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_merge_request_commits.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	_, resp, err := c.GetMergeRequestCommits("12345678", 123456)

	assert.Error(t,err,"Asserting there is an error returned from Merge Request Commits with a bad request body")
	assert.Equal(t , "404 Not Found" , resp.Status, "Assert Response is 404")
	log.Print(err)
}

func TestGitLabAPIService_ListCommitsBetweenTimeStamps(t *testing.T) {
	time1 := time.Now()

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/repository/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_commits_between_timestamps.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	time2 := time.Now()

	commits, resp, err := c.ListCommitsBetweenTimeStamps("12345", &time1, &time2)

	assert.NoErrorf(t,err,"Asserting there is no error returned from Merge Request Commits")
	assert.Equal(t , "200 OK" , resp.Status, "Assert Response is 200")
	assert.Equal(t, "6104942438c14ec7bd21c6cd5bd995272b3faff6", commits[0].ID, "Asserting order is maintained.")
	log.Print(err)
}

func TestGitLabAPIService_ListCommitsBetweenTimeStamps_Failure(t *testing.T) {
	time1 := time.Now()

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/repository/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_commits_between_timestamps.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	time2 := time.Now()

	_, resp, err := c.ListCommitsBetweenTimeStamps("12345789", &time1, &time2)

	assert.Errorf(t,err,"Asserting there is no error returned from Merge Request Commits")
	assert.Equal(t , "404 Not Found" , resp.Status, "Assert Response is 200")

	log.Print(err)
}

func TestGitLabAPIService_ListProjectDeployments(t *testing.T) {
	time1 := time.Now()

	mux, server, client := setup()
	defer teardown(server)

	path := "/api/v4/projects/12345/deployments"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_list_project_deployments.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	deployments, resp, err := c.ListProjectDeployments("12345", time1.String(), 1)

	assert.NoErrorf(t,err,"Asserting there is no error returned from Merge Request Commits")
	assert.Equal(t , "200 OK" , resp.Status, "Assert Response is 200")
	assert.Equal(t, 41, deployments[0].ID, "Asserting most recent deployment is returned first.")
	log.Print(err)
}

func TestGitLabAPIService_ListProjectDeployments_faulure(t *testing.T) {
	time1 := time.Now()

	mux, server, client := setup()
	defer teardown(server)

	//set up wrong path
	path := "/api/v4/projects/12345/commits"

	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		writeHTTPResponse(t, w, "testdata/get_list_project_deployments.json")
	})

	c := &GitLabAPIService{
		Client: client,
	}

	_, resp, err := c.ListProjectDeployments("12345", time1.String(), 1)

	assert.Errorf(t,err,"Asserting there is an error returned from List Project Deployments")
	assert.Equal(t , "404 Not Found" , resp.Status, "Assert Response is 404")
	log.Print(err)
}

func TestGitLabAPIService_SetAPIToken(t *testing.T) {
	g := &GitLabAPIService{}

	fakeToken := "THIS_IS_A_FAKE_TOKEN"
	assert.Equal(t, "", g.apiToken, "Api token should be blank in order to proceed with test")

	g.SetAPIToken(fakeToken)

	assert.Equal(t, g.apiToken, fakeToken, "APIToken should be updated after SetAPIToken call is made")

}

func TestGitLabAPIService_NewService_APITokenSet(t *testing.T) {

	comparatorAPIToken := "fakeToken01010101"

	comparatorClient := gitlab.NewClient(&http.Client{}, comparatorAPIToken)

	g := &GitLabAPIService{
		apiToken: comparatorAPIToken,
		Client: comparatorClient,
	}

	err := g.NewService()

	if err != nil{
		t.Fail()
		log.Print(err)
	}

	assert.Equal(t, comparatorClient, g.Client, "type Gitlab.Client in struct GitLabAPIService should equal comparator client created in test when valid NewService call is made")

}

func TestGitLabAPIService_NewService_APITokenNotSet(t *testing.T) {

	dummyAPIToken := "fakeToken01010101"

	dummyClient := gitlab.NewClient(&http.Client{}, dummyAPIToken)

	g := &GitLabAPIService{
		apiToken:"",
		Client: dummyClient,
	}

	err := g.NewService()

	assert.Errorf(t, err, "Error Should be returned if NewService is called without setting Apitoken")

}

func setup() (*http.ServeMux, *httptest.Server, *gitlab.Client) {
	// mux is the HTTP request multiplexer used with the test server.
	mux := http.NewServeMux()

	// server is a test HTTP server used to provide mock API responses.
	server := httptest.NewServer(mux)

	// client is the Gitlab client being tested.
	client := gitlab.NewClient(nil, "")
	client.SetBaseURL(server.URL)

	return mux, server, client
}

// teardown closes the test HTTP server.
func teardown(server *httptest.Server) {
	server.Close()
}

func testMethod(t *testing.T, r *http.Request, want string) {
	if got := r.Method; got != want {
		t.Errorf("Request method: %s, want %s", got, want)
	}
}

func writeHTTPResponse(t *testing.T, w http.ResponseWriter, s string) {
	f, err := os.Open(s)
	if err != nil {
		t.Fatalf("error opening fixture file: %v", err)
	}

	if _, err = io.Copy(w, f); err != nil {
		t.Fatalf("error writing response: %v", err)
	}
}
