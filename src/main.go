package main

import (
	"flag"
	"fmt"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/controllers"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"net"
)
// Configuration options
var (
	brokerDomain string
	brokers      string
	version      string
	group        string
	topics       string
	assignor     string
	oldest       bool
	debug        bool
	verbose      bool
	esDomain string
	esURLs   string
	gitlabAPIToken string
)

func main() {
	parseFlags()
	dnsLookups()
	kafkaController := initKafka(*initReactor(*initCycleTime(*initGitlab(),*initElastic())))
	kafkaController.StartConsumerGroup()
}

func initElastic() *controllers.ElasticSearchController {
	elasticSearchController := controllers.NewElasticSearchController("cycle_time", "https://" +esURLs, "cycle_time", false, false)
	elasticSearchController.Connect()
	return elasticSearchController
}

func initGitlab() *controllers.GitlabController {
	gitlabController := controllers.NewGitlabController(gitlabAPIToken)
	gitlabController.NewService()
	return gitlabController
}

func initCycleTime(gitlabController controllers.GitlabController, elasticSearchController controllers.ElasticSearchController) *controllers.CycleTimeCalculator{
	cycleTimeCalculator := controllers.NewCycleTimeCalculator(gitlabController, elasticSearchController)
	return cycleTimeCalculator
}

func initReactor(cycleTimeCalculator controllers.CycleTimeCalculator) *controllers.MessageReactor {
	messageReactor := controllers.NewMessageReactor(&cycleTimeCalculator)
	return messageReactor
}

func initKafka(reactor controllers.MessageReactor) *controllers.KafkaController {
	kafkaController := &controllers.KafkaController{}
	kafkaController.InitializeConfiguration(brokers,topics,group,version,assignor,oldest)
	kafkaController.SetReactor(&reactor)
	kafkaController.SetVerbosity(verbose)
	return kafkaController
}



func parseFlags() {
	flag.StringVar(&brokers, "brokers", "", "Kafka bootstrap brokers to connect to, as a comma separated list")
	flag.StringVar(&group, "group", "", "Kafka consumer group definition")
	flag.StringVar(&version, "version", "2.1.1", "Kafka cluster version")
	flag.StringVar(&topics, "topics", "", "Kafka topics to be consumed, as a comma seperated list")
	flag.StringVar(&assignor, "assignor", "range", "Consumer group partition assignment strategy (range, roundrobin, sticky)")
	flag.BoolVar(&oldest, "oldest", true, "Kafka consumer consume initial offset from oldest")
	flag.BoolVar(&verbose, "verbose", false, "Sarama logging")
	flag.BoolVar(&verbose, "debug", false, "Debug output for logging")
	flag.StringVar(&gitlabAPIToken, "gl-token", "", "Gitlab API Token for querying GitLab API")
	flag.StringVar(&esDomain, "es-domain", "", "Comma Seperated list of ElasticSearch URLS to commit cycle time too")
	flag.StringVar(&brokerDomain, "kafka-domain", "metrics-kafka-brokers.cdp-npe.t-mobile.com", "Domain to Resolve Kafka")
	flag.Parse()
}

func dnsLookups() {
	brokers = dnsTxtRecordLookup(brokerDomain)

	esURLs = dnsTxtRecordLookup(esDomain)

	if len(brokers) == 0 {
		util.Panic("no Kafka bootstrap brokers found")
	}

	if len(topics) == 0 {
		util.Panic("no topics given to be consumed, please set the -topics flag")
	}

	if len(group) == 0 {
		util.Panic("no Kafka consumer group defined, please set the -group flag")
	}

	if len(esURLs) == 0 {
		util.Panic("no Elastic Search endpoint found, please set -es-domain flag")
	}

}

func dnsTxtRecordLookup(recordName string) (recordValue string) {
	txtrecords, err := net.LookupTXT(recordName)
	if err != nil {
		err := fmt.Errorf("[ERROR] Failed retrieving DNS TXT record %s", recordName)
		util.LogErr("Error while attempting to look up dns records", err)
		return ""
	}
	if len(txtrecords) == 0 {
		err := fmt.Errorf("[ERROR] Failed DNS TXT record %s not found", recordName)
		util.LogErr("Error while attempting to look up dns records", err)
		return ""
	}
	if len(txtrecords) > 1 {
		err := fmt.Errorf("[ERROR] Multiple %s DNS TXT records found, expecting 1", recordName)
		util.LogErr("Error while attempting to look up dns records", err)
		return ""
	}

	return txtrecords[0]
}
