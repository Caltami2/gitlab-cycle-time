package util

import "log"


//LogErr expects a string and an error and logs the formatted error using log.Printf
func LogErr(s string, err error){
	log.Printf("[ERROR] %v : %v", s, err)
}
//LogWarn expects a string and logs the formatted error using log.Printf
func LogWarn(s string){
	log.Printf("[WARNING] %v", s)
}

//LogInfo expects a string and logs the formatted error using log.Printf
func LogInfo(s string){
	log.Printf("[INFO] %v", s)
}
//Panic expects a string and results in the application exiting due to a panic.
func Panic(s string){
	log.Panic(s)
}