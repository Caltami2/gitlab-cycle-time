module gitlab.com/tmobile/cdp/gitlab-cycle-time

go 1.13

require (
	github.com/Shopify/sarama v1.26.1
	github.com/abdallahmaltamimi/go-gitlab v0.22.4
	github.com/golang/mock v1.2.0
	github.com/olivere/elastic/v7 v7.0.10
	github.com/stretchr/testify v1.4.0
	github.com/vburenin/ifacemaker v1.1.0 // indirect
)
