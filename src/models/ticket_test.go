package models

import (
	"fmt"
	"log"
	"reflect"
	"testing"
	"time"
)

func TestConstructTicket(t *testing.T) {
	staticTimestamp := time.Now()
	type args struct {
		ticket    string
		timestamp *time.Time
		suffix    string
	}
	tests := []struct {
		name string
		args args
		want Ticket
	}{
		{
			name:"Test Construct Ticket",
			args : args {
				ticket:"TICKET-1111",
				timestamp: &staticTimestamp,
				suffix: ".started",
			},
			want : Ticket{
				TicketName: "TICKET-1111.started",
				TimeStamp: &staticTimestamp,

			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConstructTicket(tt.args.ticket, tt.args.timestamp, tt.args.suffix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ConstructTicket() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConstructTickets(t *testing.T) {
	staticTimestamp := time.Now()
	type args struct {
		tickets   []string
		timestamp *time.Time
		suffix    string
	}
	tests := []struct {
		name string
		args args
		want []Ticket
	}{
		{
			name:"Test Construct Ticket",
			args : args {
				tickets: []string{
					"TEST-TICKET-1",
					"TEST-TICKET-2",
					"TEST-TICKET-3",
				},
				timestamp: &staticTimestamp,
				suffix: ".stopped",
			},
			want : []Ticket{{
				TicketName: "TEST-TICKET-1.stopped",
				TimeStamp: &staticTimestamp ,
			},
			{
				TicketName: "TEST-TICKET-2.stopped",
				TimeStamp: &staticTimestamp ,
			},
			{
				TicketName: "TEST-TICKET-3.stopped",
				TimeStamp: &staticTimestamp ,
			},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConstructTickets(tt.args.tickets, tt.args.timestamp, tt.args.suffix); !reflect.DeepEqual(got, tt.want) {
				log.Println(fmt.Sprintf("Test failed: %v %v", got, tt.want))
				t.Errorf("ConstructTickets() = %v, want %v", got, tt.want)
			}
		})
	}
}