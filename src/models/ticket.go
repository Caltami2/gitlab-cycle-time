package models

import (
	"fmt"
	"gitlab.com/tmobile/cdp/gitlab-cycle-time/util"
	"time"
)

//Ticket represents a "Ticket" from jira. This "Ticket" represents a either cycletime start or stop.
type Ticket struct {
	TicketName string     `json:"ticket"`
	TimeStamp  *time.Time `json:"time_stamp"`
}

//ConstructTickets constructs an array of tickets using the specified timestamp. (plural)
func ConstructTickets(tickets []string, timestamp *time.Time, suffix string) []Ticket{
	//Notes: ConstructTickets requires a valid timestamp that can be parsed.
	var constructedTickets []Ticket
	for ticket := range tickets{
		constructedTickets = append(constructedTickets,ConstructTicket(tickets[ticket], timestamp, suffix))
		util.LogInfo(fmt.Sprintf("Constructed ticket : %v with timestamp : %v with the suffix : %v", tickets[ticket], timestamp.String(),suffix))
	}
	return constructedTickets
}

//ConstructTicket constructs a single Ticket using the specified timestamp.
func ConstructTicket(ticket string, timestamp *time.Time, suffix string) Ticket{

	//Notes: ConstructTicket requires a valid timestamp that can be parsed.
	return Ticket{
		TicketName: ticket + suffix,
		TimeStamp: timestamp,
	}
}