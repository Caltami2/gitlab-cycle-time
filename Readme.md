# Cycle Time Record Generator

The Goal for this tool is to listen to webhooks as they are added to Kafka, and be able to
call onto Gitlab to parse out a Cycle Time Start for Merge Events, and Cycle Time End
for Deploy Events


---

## Flow of events.

1)Take a (MR) Merge request or (PE) Pipeline event.
2) Then it will assert:
- (PE) Assert that the Pipeline event contains a deployment to prod.
- OR
- (MR) Take the Merge Request regardless of it’s origin
3) If either of those branches return TRUE Then we will search for any ticket names according to regex in the message / title.
4) After it gets all tickets associated with the (PE) or (MR) Then the app will:
- (PE) Add a “STOPPED” suffix to the ticket name.
- (MR) Add a “STARTED” suffix to the ticket name
5) It will insert all tickets into Elasticsearch.


## Cycle Time Start
When a Merge Request Webhook comes in, we Parse the whole of the Source Branch to find all
of the Jira Tickets Mentioned in the Commit Messages and Branch Names. These Tickets are
said to have started when the First commit of that  Branch was first authored. Once this is
found, it will be added to Elastic Search as:

`{"ticket": "FOO-102.started", "time_stamp":2019-12-22T07:20:14}`


## Cycle Time Stopped
When a Pipeline event comes in, this signals to us that something possibly deployed. We check
to see if something has been deployed into a Production Env. We Then look for the last successful production deploy, capture all of jira tickets between those deploys, and labeling
them as follows in Elastic Search:

`{"ticket": "FOO-102.stopped", "time_stamp":2019-12-22T14:01:14}`

---

## Building

make sure to build the binary as such: `CGO_ENABLED=0 go build ./src`
## Running
To Run Execute: `cycle-time -group cycle_time  -es-domain $ES_DOMAIN -topics cdp-gitlab-webhook-merge-requests,cdp-gitlab-webhook-pipelines  -gl-token $GL_API_TOKEN -kafka-domain $KAFKA_DOMAIN`

Example: `cycle-time -group cycle_time  -es-domain metrics-es.cdp.t-mobile.com  -topics cdp-gitlab-webhook-merge-requests,cdp-gitlab-webhook-pipelines -gl-token $GL_API_TOKEN -kafka-domain metrics-kafka-brokers.cdp.t-mobile.com`

---

## How can I monitor gitlab_cycle_time is running?

You can run a curl against the Elastic Search Cluster to get the running count for the `cycle_time` index.
` watch -n3 curl -s https://vpc-cdp-logging-elasticsearch-mahdmf6fyktfrjvhxs63iuix4u.us-west-2.es.amazonaws.com/cycle_time/_count`

---

## Dye Test
a dye test is a test where in you preform an action with an expectation of a result on the other end. In our case, we have provided `dye-test.sh` to achieve this

### What does it do?
The steps are as follows:
1) Clone down the hello world template
2) Append a space at the end of `README.md`
3) Create a branch with a randomly generated `helloworld` jira ticket (i.e. HEL-007)
4) Commit and push said branch to Gitlab
5) Via the API, create a Merge Request
6) Approve the merge request so that it may be merged
7) With that approval being successful, we merge that branch into `tmo/master`
8) This Merge Request will add an event to kafka, and that event will be captured in the
Cycle Time start logic, and be added to Elastic Search
9) After the branch is merged, it will trigger a deploy
10) Once that deploy finishes, it will send a Githook to kafka under the pipelines topic
11) With that Pipeline finishing, we will search for all commits between this successful
deploy, and the last successful deploy, then capture all commits in between those deploys
12) Our fake ticket ought to be in the deploy, and found given the Cycle Time Stop Logic
13) Once found, it will be tagged as stopped, and added to elastic search
14) The Dye Test will query Elastic Search for the ticket, it ought to respond with at least
One ticket, but ideally two, Cycle Time Start (HEL-007.started)
and Cycle Time Stopped (HEL-007.stopped)
