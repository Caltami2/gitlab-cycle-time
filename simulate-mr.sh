#!/bin/bash -x

### PROJECT="tmobile/cdp/big-sandbox/cycle-time-test"
### PROJECT_URLENCODED="tmobile%2Fcdp%2Fbig-sandbox%2Fcycle-time-test"  # URL encoded path to project
### PROJECT_DIR="sandbox"
PROJECT="tmobile/templates_projects/helloworld-1"  # URL encoded path to project
PROJECT_URLENCODED="tmobile%2Ftemplates_projects%2Fhelloworld-1"  # URL encoded path to project
PROJECT_DIR="helloworld-1"


API_BASE="https://gitlab.com/api/v4"
ES_ENDPOINT="https://vpc-cdp-logging-elasticsearch-mahdmf6fyktfrjvhxs63iuix4u.us-west-2.es.amazonaws.com/cycle_time"


TICKET="HELLO-$((RANDOM % 10))$((RANDOM % 10))$((RANDOM % 10))$((RANDOM % 10))"
BRANCH="dev/$TICKET"

PROJECT_REPO="gitlab.com/${PROJECT}.git"
PROJECT_INFO_JSON=$(curl -s -X GET -H "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" "${API_BASE}/projects/$PROJECT_URLENCODED")
PROJECT_ID=$(echo $PROJECT_INFO_JSON | jq -r '.id')


TMPDIR="./tmp/simulate-mr"

if [ -z "$GL_ACCESS_TOKEN" ] ; then 
    echo "[ERROR] GL_ACCESS_TOKEN NOT SET, PLEASE SET/EXPORT GL_ACCESS_TOKEN"
    exit 1
fi
if [ -z "$APPROVER_GL_ACCESS_TOKEN" ] ; then 
    echo "[ERROR] GL_ACCESS_TOKEN NOT SET, PLEASE SET/EXPORT APPROVER_GL_ACCESS_TOKEN"
    exit 1
fi


###
###
###

mkdir -p $TMPDIR
cd $TMPDIR
rm -rf $PROJECT_DIR

PROJ_DIR="$TMPDIR/$PROJECT_DIR"

echo "[INFO] Cloning $PROJECT_REPO to $PROJECT_DIR"


git clone "https://${GL_ACCESS_USER}:${GL_ACCESS_TOKEN}@${PROJECT_REPO}" $PROJECT_DIR
cd $PROJECT_DIR

git config --global user.email "$GL_ACCESS_USER@t-mobile.com"
git config --global user.name $GL_ACCESS_USER
git checkout -b $BRANCH

echo "[INFO] Updating date in testing-README.md"
date > testing-README.md 
git add testing-README.md
git commit -m "Simulated-MR test with ticket ${TICKET}"
git push origin $BRANCH

########################################
# Create MR
########################################
echo "[INFO] Creating MR"
#https://gitlab.com/tmobile/cdp/gitlab-cycle-time/-/merge_requests/new?merge_request%5Bsource_branch%5D=HELLO-447
RESPONSE=$(curl -s -X POST "$API_BASE/projects/$PROJECT_ID/merge_requests" \
		-H "PRIVATE-TOKEN: $GL_ACCESS_TOKEN" \
		-H 'Content-Type: application/json' \
		-d '{"source_branch": "'$BRANCH'","target_branch":"tmo/master","title":"Simulated-MR Test for gitlab-cycle-time '$TICKET'."}')

IID=$(echo $RESPONSE | jq -r '.iid')

########################################
# Approve MR
########################################
echo "[INFO] Approving MR"
curl -s -X POST "$API_BASE/projects/$PROJECT_ID/merge_requests/$IID/approve" -H "PRIVATE-TOKEN: $APPROVER_GL_ACCESS_TOKEN"

sleep 5s

########################################
# Merge MR
########################################
echo "[INFO] Check if MR can be merged"

RESPONSE=$(curl -s -X GET "$API_BASE/projects/$PROJECT_ID/merge_requests/$IID" -H "PRIVATE-TOKEN: $GL_ACCESS_TOKEN")
MERGE_STATUS=$(echo $RESPONSE | jq -r '.merge_status')
if [ "$MERGE_STATUS" != "can_be_merged" ]; then
    echo "[INFO] Waiting 30s and then will check again"
    COUNT=0
    MAX_COUNT=100
    while [ $COUNT -lt $MAX_COUNT ]; do
	RESPONSE=$(curl -s -X GET "$API_BASE/projects/$PROJECT_ID/merge_requests/$IID" -H "PRIVATE-TOKEN: $GL_ACCESS_TOKEN")
	MERGE_STATUS=$(echo $RESPONSE | jq -r '.merge_status')
	if [ $MERGE_STATUS = "can_be_merged" ]; then
	    break
	fi
	COUNT=$((COUNT + 1))
    done
fi
if [ $MERGE_STATUS != "can_be_merged" ]; then
    echo "[ERROR] Cannot merge"
    exit 2
fi


echo "[INFO] Merge MR"
RESPONSE=$(curl -s -X PUT "$API_BASE/projects/$PROJECT_ID/merge_requests/$IID/merge" -H "PRIVATE-TOKEN: $GL_ACCESS_TOKEN")

MSG=$(echo $RESPONSE | jq -r '.message')
if [ "$MSG" != "null" ]; then
    echo "[ERROR] Failed merge: $MSG"
    exit 1
fi


########################################
# Check ES
########################################
sleep 5s

echo "Querying ES for $TICKET.started"
if curl -s -X POST "$ES_ENDPOINT/_search?q=$TICKET.started" | eval grep $TICKET.started 
then
    echo "$TICKET.started successfully found in elasticsearch"
else
    echo "Failed to find elastic search record for ticket.stopped"
    exit 1
fi


end=$((SECONDS+1200))

while [ $SECONDS -lt $end ]; do
    echo "Querying ES for $TICKET.stopped"
    if curl -s -X POST "$ES_ENDPOINT/_search?q=$TICKET.stopped"  | eval grep $TICKET.stopped 
    then
	echo "$TICKET.stopped successfully found in elasticsearch, test successful."
	exit 0
    else
	sleep 5s
    fi
done

echo "Test timed out trying to find ticket in elasticsearch. Failure."

exit 1
