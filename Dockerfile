FROM alpine:latest
RUN apk --update add ca-certificates
COPY *.crt /usr/local/share/ca-certificates/

WORKDIR /app/
COPY gitlab-cycle-time .
ENTRYPOINT ["/app/gitlab-cycle-time"]
