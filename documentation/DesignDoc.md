# Summary

The purpose of this effort is track cycle time for all Jira projects. 
Cycle time will be calculated by this tool and stored for all Developers, Product owners and others to view and report on.
The problem this project is intended to solve is understanding the cycle time of features and functionality deployed into
the production environment of T-Mobile.  

# Problem Statement

This section should document all the problems that the effort is intended to address or partially address, to show what motivated the design documented in this file.  It should contain the _original_ problem statements, and link to any Jira stories that the problem statements were drawn from.

![Problem statement image](img/design.png "Design of Cycle_time calculation ")

## Problem Discussion

This is an optional sub-section of the *Problem Statement* section, and is intended for the design proposer to explain how they interpreted the problem statements and how those problem statements may have evolved since they were originally proposed in a Jira ticket.  Whereas the primary *Problem Statement* section conceptually contains a list, this sub-section should consist of prose, and should further motivate the design.

# Design

Simply, the application will:

1. Read from Kafka topics for PE and MR
3. Read data from Gitlab for commits in Pipeline Event
2. Collect jira tickets mentioned in either
3. Write data to Elasticsearch 

However, there are some branches to the logic. The logic is as follows.

(1)Take a (MR) Merge request or (PE) Pipeline event.

(2) Then it will assert:
- (PE) Assert that the Pipeline event contains a deployment to prod.
- OR
- (MR) Take the Merge Request regardless of it’s origin

(3) If either of those branches return TRUE Then we will search for any ticket names according to regex in the message / title.

(4) After it gets all tickets associated with the (PE) or (MR) Then the app will:

- (PE) Add a “STOPPED” suffix to the ticket name.
- (MR) Add a “STARTED” suffix to the ticket name

(5) It will insert all tickets into Elasticsearch.


### Cycle Time Start
When a Merge Request Webhook comes in, we Parse the whole of the Source Branch to find all
of the Jira Tickets Mentioned in the Commit Messages and Branch Names. These Tickets are
said to have started when the First commit of that  Branch was first authored. Once this is
found, it will be added to Elastic Search as:

`{"ticket": "FOO-102.started", "time_stamp":2019-12-22T07:20:14}`


### Cycle Time Stopped
When a Pipeline event comes in, this signals to us that something possibly deployed. We check
to see if something has been deployed into a Production Env. We Then look for the last successful production deploy, capture all of jira tickets between those deploys, and labeling
them as follows in Elastic Search:

`{"ticket": "FOO-102.stopped", "time_stamp":2019-12-22T14:01:14}`


## Debugging, Building, or running the Application
### Building the Go binary and Running the application

Make sure to build the binary as such: `CGO_ENABLED=0 go build ./src`

To Run Execute: `cycle-time -group cycle_time  -es-domain $ES_DOMAIN -topics cdp-gitlab-webhook-merge-requests,cdp-gitlab-webhook-pipelines  -gl-token $GL_API_TOKEN -kafka-domain $KAFKA_DOMAIN`

Example: `cycle-time -group cycle_time  -es-domain metrics-es.cdp.t-mobile.com  -topics cdp-gitlab-webhook-merge-requests,cdp-gitlab-webhook-pipelines -gl-token $GL_API_TOKEN -kafka-domain metrics-kafka-brokers.cdp.t-mobile.com`

### How can I debug the application ?

You can use your favorite editor to run the application in Debug mode and insert breakpoints, otherwise you can use the methods below to directly query elasticsearch.

### How can I see that there are tickets being created?

You can run a curl against the Elastic Search Cluster to get the running count for the `cycle_time` index. Or you can hit elasticsearch using your own methods.
` watch -n3 curl -s https://vpc-cdp-logging-elasticsearch-mahdmf6fyktfrjvhxs63iuix4u.us-west-2.es.amazonaws.com/cycle_time/_count`


# Proposed Stories

Stories in this Jira 
https://jirasw.t-mobile.com/secure/RapidBoard.jspa?rapidView=4183&view=detail&selectedIssue=TEQCDP-2055&quickFilter=10206&quickFilter=10209

labels = AutoCycleTimeMetric
