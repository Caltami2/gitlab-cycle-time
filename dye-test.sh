#!/bin/bash -x
ticket="HEL-$((RANDOM % 10))$((RANDOM % 10))$((RANDOM % 10))"

# HEL="git@tmo:tmobile/templates_projects/helloworld-1.git"
HEL="git@gitlab.com:tmobile/templates_projects/helloworld-1.git"
API_BASE="https://gitlab.com/api/v4/projects/tmobile%2Ftemplates_projects%2Fhelloworld-1"
ES_ENDPOINT="https://vpc-cdp-logging-elasticsearch-mahdmf6fyktfrjvhxs63iuix4u.us-west-2.es.amazonaws.com/cycle_time"


if [ -z $GL_ACCESS_TOKEN ] ; then 
  echo "GL_ACCESS_TOKEN NOT SET, PLEASE SET/EXPORT GL_ACCESS_TOKEN"
  exit 1
fi

echo "Dye Testing with ticket $ticket"

cd /tmp
echo "Cloning $HEL"
git clone  http://$GL_ACCESS_USER:$GL_ACCESS_TOKEN@gitlab.com/tmobile/templates_projects/helloworld-1.git

if [ ! -d /tmp/helloworld-1 ] ; then
  echo "Git Clone failed, or other issue. Please see about. Exiting..."
  exit 1
fi

cd /tmp/helloworld-1
git checkout -b $ticket

echo "Appending a space to Readme"
echo " " >> README.md 
git add README.md
git config --global user.email "$GL_ACCESS_USER@t-mobile.com"
git config --global user.name $GL_ACCESS_USER
git commit -m "dye test with ticket ${ticket}"
git push origin $ticket

echo "Creating MR"
iid=$(curl -X POST $API_BASE/merge_requests\?access_token=$GL_ACCESS_TOKEN \
  --header 'Content-Type: application/json' -d '{"source_branch": "'$ticket'","target_branch": "tmo/master","title": "Dye Test for GL_CT"}'| jq -r '.iid')

if [ -z $iid ] ; then
  echo "Opening MR Failed, IID not found. Check Access"
  exit 1
fi 


echo "iid=$iid"
echo "Sleep 5"


echo "Approving MR"
curl -s -X POST "$API_BASE/merge_requests/$iid/approve" --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN"

sleep 5s

echo "Merging MR"
curl -s -X PUT "$API_BASE/merge_requests/$iid/merge" --header "PRIVATE-TOKEN: $GL_ACCESS_TOKEN"

sleep 30s

echo "Querying ES for $ticket.started"
if curl -s -X POST "$ES_ENDPOINT/_search?q=$ticket.started" | eval grep $ticket.started 
then
echo "$ticket.started successfully found in elasticsearch"
else
echo "Failed to find elastic search record for ticket.stopped"
exit 1
fi


#! /bin/bash
end=$((SECONDS+1200))

while [ $SECONDS -lt $end ]; do
    echo "Querying ES for $ticket.stopped"
    if curl -s -X POST "$ES_ENDPOINT/_search?q=$ticket.stopped"  | eval grep $ticket.stopped 
    then
    echo "$ticket.stopped successfully found in elasticsearch, test successful."
    exit 0
    else
    sleep 30s
    fi
done

echo "Test timed out trying to find ticket in elasticsearch. Failure."

exit 1


